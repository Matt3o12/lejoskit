package de.matt3o12.lejoskit;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;

import lejos.nxt.LCD;

/**
 * Called when the program crashes and it couldn't be handled. It saves the file
 * the throwable on the disk (warning it might be hard to read because of the way
 * NXT saves exception) and leaves a note on the screen.
 */
public class CrashReporter {
	/** The beginning of the name of the crash report ("crash_repprt"). */
	public static final String CRASHFILE_PREFIX = "crash_report";
	
	/** The ending of the name of the crash report (".txt"). */
	public static final String CRASHFILE_SUFFIX = ".txt";
	
	/**
	 * Number of crash reports to keep. Since the NXT can only store
	 * 20 files (as of Jun 1st, 2014), this number should be low. When
	 * the limit of 20 files is exceeded, an exception will be thrown.
	 * 
	 * If the {@link #MAX_CRASH_REPORTS} limit is exceted, the latest
	 * crash report will be replaced by the current one.
	 */
	public static final int MAX_CRASH_REPORTS = 5;
	
	private Throwable crash;
	
	/**
	 * Creates a new {@link CrashReporter}.
	 * 
	 * @param crash the crash to occur.
	 */
	public CrashReporter(Throwable crash) {
		setThrowable(crash);
	}
	
	/**
	 * Sets the {@link Throwable} to occur.
	 * 
	 * @param crash the {@link Throwable} that has occurred.
	 */
	protected void setThrowable(Throwable crash) {
		this.crash = crash;
	}
	
	/**
	 * Generates a file name for the crash report file. The result will look
	 * like: "crash_report1.txt". This method takes {@link #CRASHFILE_PREFIX},
	 * {@link #CRASHFILE_SUFFIX}, and {@link #MAX_CRASH_REPORTS} into account.
	 * 
	 * @return the file name to use for the crash report.
	 */
	public String getCrashFileName() {
		File[] files = getFiles();
		int higestNumber = 0;
		if (files != null) {
			for (File file : files) {
				if (file == null)
					continue;
				
				String fName = file.getName();
				int suffixPos = fName.length() - CRASHFILE_SUFFIX.length();
				if (!(fName.startsWith(CRASHFILE_PREFIX)
						&& fName.startsWith(CRASHFILE_SUFFIX, suffixPos))) {
					continue;
				}

				String number = fName.substring(0, suffixPos);
				number = number.substring(CRASHFILE_PREFIX.length());
				
				try {
					int n = Integer.parseInt(number);
					if ((n > higestNumber) && (MAX_CRASH_REPORTS > n))
						higestNumber = n;
					else if (MAX_CRASH_REPORTS > n)
						higestNumber = MAX_CRASH_REPORTS - 1;
				} catch (NumberFormatException e) {
					
				}
			}			
		}
		
		return CRASHFILE_PREFIX + (higestNumber + 1) + CRASHFILE_SUFFIX;
	}
	
	/**
	 * Returns all file in the crash directory.
	 * 
	 * @return all files
	 */
	public File[] getFiles() {
		return File.listFiles();
	}
	
	/**
	 * Reports the error. A message saying an error occurred will be shown
	 * on the screen and the report will be saved into the crash report file.
	 * 
	 * This method will block any never return (except the current {@link Thread}
	 * is interrupted).
	 */
	public synchronized void report() {
		report(false);
	}
	
	/**
	 * Saves the report on the disk and prints a message on the screen. 
	 * This method will block unless returnImmediately is true.
	 * 
	 * @param returnImmediately true if the method shouldn't block.
	 */
	public synchronized void report(boolean returnImmediately) {
		String crashFileName = getCrashFileName();
		PrintStream ps = null;
		FileOutputStream os = null;
		try {
			File crashFile = new File(crashFileName);
			os = new FileOutputStream(crashFile);
			ps = new PrintStream(os);
			crash.printStackTrace(ps);
			
			// Don't use DisplayUnit since the error might have happened
			// there.
			LCD.clear();
			drawMessages("An unexpceted", "error occured.",
					"Details were", "stored in", crashFileName);
		} catch (IOException ex) {
			// Since it isn't possible to write, just throw the given
			// Throwable as a runtime exception
			throw new RuntimeException(crash);
		} finally {
			if (ps != null) {
				try {
					ps.close();
				} catch (IOException e) {
					// Only thrown if the stream is already closed
				}				
			}
			
			if (os != null) {
				try {
					os.close();
				} catch (IOException e) {
					// Only thrown if the stream is already closed
				}
			}
		}
		
		if (!returnImmediately) {
			try {
				Thread.sleep(Long.MAX_VALUE);
			} catch (InterruptedException e) {
				
			}
		}
	}
	
	private void drawMessages(String... messages) {
		for (int i = 0; i < messages.length; i++) {
			String message = messages[i];
			LCD.drawString(message, 0, i);
		}
	}
}
