package de.matt3o12.lejoskit;

import java.util.ArrayList;
import java.util.Iterator;

import javax.microedition.lcdui.Display;

import lejos.nxt.LCD;

/**
 * Useful functions for things you can do with the LCD {@link Display}.
 */
public class DisplayUnit {
	/** The width of the display (16). */
	public static final int DISPLAY_WIDTH = 16;
	
	/** The depth of the display (8). */
	public static final int DISPLAY_DEPTH = 8;
	
	/** The depth of the display in float (8f). */
	public static final float DISPLAY_DEPTHf = 8f;
	
	/** The width of the display in float (16f). */
	public static final float DISPLAY_WIDTHf = 16f;
	
	/**
	 * The stings used to invert the string
	 * (see: {@link LCD#drawString(String, int, int, boolean)}). 
	 */
	public static final String INVERT_CHAR = "\\i";
	
	private static final int LONG_WORD_LENGTH = 12;
	
	/**
	 * Splits that given parameter string so that it fits on 
	 * the screen of the NXT.  
	 * It throws an {@link ArrayIndexOutOfBoundsException} if 
	 * the string doesn't fit on the screen.  
	 * 
	 * That's how the algorithm works:
	 * The display is always 16 characters wide and 8 characters deep.
	 * First it splits the string into words. For instance, the
	 * sentence `  hello world. I'm a  test ` will be split into:
	 * `hello`, `world.`, `I'm`, `a`, and `test`. Points won't be lost. 
	 * Leading and trailing white spaces will be ignored. If there were
	 * more than 2 white spaces between 2 words, they will be trimmed to 
	 * one.  
	 * Now, it will try to add as many words as possible to the the first
	 * row. If the length of the current row + the length of the current
	 * word is longer than 16, the word will be added to the next row.  
	 * However, if the word is longer than 12 characters, and it doesn't
	 * fit on the current row, it will be hyphenated. Hyphenating rules of
	 * the english language will be ignored.  
	 *
	 * @param string The given string. Should only contain white spaces
	 * @return An array with all string. The array will always have
	 * 			8 string or null.
	 * @throws ArrayIndexOutOfBoundsException if the string took
	 * 			more than 8 rows.
	 * @see #splitString(String, int)
	 */
	public String[] splitString(String string) throws ArrayIndexOutOfBoundsException {
		return splitString(string, DISPLAY_DEPTH);
	}
	
	/**
	 * Splits that given parameter string so that it fits on 
	 * the screen of the NXT.  
	 * It throws an {@link ArrayIndexOutOfBoundsException} if 
	 * the string took more than rows than allowed in length.
	 * 
	 * That's how the algorithm works:
	 * The display is always 16 characters wide and 8 characters deep.
	 * First it splits the string into words. For instance, the
	 * sentence `  hello world. I'm a  test ` will be split into:
	 * `hello`, `world.`, `I'm`, `a`, and `test`. Points won't be lost. 
	 * Leading and trailing white spaces will be ignored. If there were
	 * more than 2 white spaces between 2 words, they will be trimmed to 
	 * one.  
	 * Now, it will try to add as many words as possible to the the first
	 * row. If the length of the current row + the length of the current word
	 * is longer than 16, the word will be added to the next row.  
	 * However, if the word is longer than 12 characters, and it doesn't fit on
	 * the current row, it will be hyphenated. Hyphenating rules of the english
	 * language will be ignored.  
	 *
	 * @param string The given string. Should only contain white spaces
	 * @param length The maximal length that the result is allowed to take.
	 * 
	 * @return An array with all string. The array will always have `lenght` string.
	 * 			Remember that string can be null.
	 * @throws ArrayIndexOutOfBoundsException if the string took more than 8 rows.
	 * @throws IllegalArgumentException thrown if is length smaller or equals to 0.
	 */
	public String[] splitString(String string, int length)
			throws ArrayIndexOutOfBoundsException, IllegalArgumentException {
		
		if (length <= 0)
			throw new IllegalArgumentException("length needs to be greater than 0");
		
		// Removing double white spaces and splitting the string
		// String.split isn't available
		StringBuilder lastWord = new StringBuilder();
		ArrayList<String> foundWords = new ArrayList<String>();
		for (int i = 0; i < string.length(); i++) {			
			char currentChar = string.charAt(i);
			if (currentChar == ' ') {
				if (!lastWord.toString().equals("")) {
					foundWords.add(lastWord.toString().trim());					
				}
				
				lastWord = new StringBuilder();
			} else {
				lastWord.append(currentChar);
			}
		}
		foundWords.add(lastWord.toString().trim()); // Adding the last word

		/* The display is 16 characters wide and 8 characters deep.
		 * The following part tries to display the string intelligently,
		 * on the display using the following algorithm:
		 *  - Try to add it to the display. If the string of the current
		 *    row exceeds 16 chars, add the string to the next row.
		 *    Exception: if the string is longer or equal to 12 characters
		 *    	(and if the string doesn't fit on the current row), add as
		 *    	many characters as possible to the current row + '-'.
		 *    	And proceed until the end of the current word is reached.
		 *      At least 2 characters have to be before '-'. If not, the
		 *      next row will be used.
		 */
		
		String[] result = new String[length];
		Iterator<String> allWords = foundWords.iterator();
		StringBuilder currentRow = null;
		int row = 0;
		String theWord = null;
		while (true) {
			if (currentRow == null)
				currentRow = new StringBuilder();
			
			boolean appendNewWord = false; 
			if (allWords.hasNext() || theWord != null) {
				if (theWord == null)
					theWord = allWords.next();
				
				appendNewWord = (currentRow.length() + theWord.length()) <= DISPLAY_WIDTH;
				if (!appendNewWord && (theWord.length() >= LONG_WORD_LENGTH)) {
					// Split word if it's too long
					
					int usedLength = DISPLAY_WIDTH - currentRow.length() - 1;
					String s = theWord.substring(0, usedLength) + "-";
					theWord = theWord.substring(usedLength);
					currentRow.append(s);
				}
				
				if (appendNewWord) {
					currentRow.append(theWord).append(" ");						
					theWord = null;
				} 
			}

			if (!(appendNewWord && allWords.hasNext())) {
				String trimmedRow = currentRow.toString().trim();
				if (!trimmedRow.equals(""))
					result[row++] = trimmedRow;
				
				currentRow = null;
			}				
			
			if ((theWord == null) && (!allWords.hasNext()))
				break;
		}
		
		return result;
	}
	
	/**
	 * Draws the given string on the display.
	 * {@link #splitString(String)} will be used to
	 * split the string.
	 * 
	 * @param string The string to be shown.
	 * @throws MessageTooLongException Thrown if the message took more than 8 rows.
	 */
	public void drawString(String string) throws MessageTooLongException {
		String[] stringSplit;
		try {
			stringSplit = splitString(string);
		} catch (ArrayIndexOutOfBoundsException e) {
			throw new MessageTooLongException(string, DISPLAY_DEPTH);
		}
		
		drawStringsOnDisplay(stringSplit);
	}
	
	/**
	 * Draws the given strings on the display.  
	 * The string can be as long as you want (no exception will be thrown) but
	 * if it is longer than 8, the NXT will just ignore the other rows.
	 * 
	 * @param strings The string to be displayed
	 */
	public void drawStringsOnDisplay(String[] strings) {
		LCD.clear();
		boolean inverted = false;
		for (int i = 0; i < strings.length; i++) {
			String row = strings[i];
			
			int invertedCharPos;
			if ((row != null) && ((invertedCharPos = row.indexOf(INVERT_CHAR)) > -1)) {
				int index = 0;
				int x = 0;
				do {
					LCD.drawString(row.substring(index, invertedCharPos), x, i, inverted);
					
					inverted = !inverted;
					x += invertedCharPos - index;
					index = invertedCharPos + INVERT_CHAR.length();
				} while ((invertedCharPos = row.indexOf(INVERT_CHAR, index)) > -1);
				
				LCD.drawString(row.substring(index, row.length()), x, i, inverted);
			} else if (row != null) {
				LCD.drawString(row, 0, i, inverted);	
			}			
		}
	}
	
	/**
	 * Centers the string.  
	 * 
	 * @param string The string to be centered
	 * @return the centered string.
	 */
	public String centerString(String string) {
		// (displayLength / 2) - (string.length() / 2) = (16/2) - (string.length() / 2)
		StringBuilder result = new StringBuilder();
		int offset = getCenterStringOffset(string);
		for (int i = 0; i < offset; i++)
			result.append(' ');
		
		result.append(string);
		return result.toString();
	}
	
	/**
	 * Returns where the centered string should when it is centered..
	 * 
	 * @param string the string to center
	 * @return might be lower than 0 if the string is longer than {@value #DISPLAY_DEPTH}.
	 */
	public int getCenterStringOffset(String string) {
		String clearedString = clearRow(string);
		return 	((int) (DISPLAY_DEPTHf - (clearedString.length() / 2f)));
	}
	
	/**
	 * Display the dialog on the screen.  
	 * The message can only take 5 row or less. Only 2 inputs are allowed.  
	 * The 2 inputs will be displayed in the very last row. The first input
	 * will be placed in the left corner and the other one will be placed in
	 * the right corner. They will be separated with as many white spaces as
	 * needed. The length of both inputs strings cannot be longer than 15
	 * (remember at least one space will be needed to separate them).   
	 * "Yes" and "No" with the message "are you
	 * sure" will be displayed like this: 
	 * 
	 * 	 ________________
	 * 	|Are you sure?  |
	 * 	|               |
	 * 	|               |
	 * 	|               |
	 * 	|               |
	 * 	|               |
	 * 	|               |
	 * 	|yes          no|
	 *      
	 * 
	 * @param message
	 *            The message to be displayed ({@link #splitString(String)} will
	 *            be used for formatting).
	 * @param inputs
	 *            2 option the user can select (e.g. "yes", "no").
	 * @throws MessageTooLongException
	 *             thrown if the given message took more than 5 rows. If the 2
	 *             inputs can be displayed in one row.
	 * @throws IllegalArgumentException
	 *             thrown unless 2 inputs were given.
	 */
	@Deprecated
	public void showDialog(String message, String[] inputs)
			throws MessageTooLongException, IllegalArgumentException {
		drawStringsOnDisplay(makeDialog(message, inputs));
	}
	
	@Deprecated
	private static final int DIALOG_MESSAGE_ROWS = 6;
	
	/**
	 * Creates a dialog as in {@link #showDialog(String, String[])} described,
	 * however it doesn't display it. It returns the strings that are necessary
	 * for {@link #drawStringsOnDisplay(String[])} to display the dialog. This
	 * method is used for unit testing the algorithm.
	 * 
	 * @param message
	 *            The message to be used.
	 * @param inputs
	 *            The inputs to be used.
	 * @return A string array the {@link #drawStringsOnDisplay(String[])} uses
	 *         to display the string
	 * @throws MessageTooLongException
	 *             thrown if the given message took more than 5 rows. If the 2
	 *             inputs can be displayed in one row.
	 * @throws IllegalArgumentException
	 *             thrown unless 2 inputs were given.
	 * @see #showDebugDisplay()
	 */
	@Deprecated
	public String[] makeDialog(String message, String[] inputs)
			throws MessageTooLongException, IllegalArgumentException {
		if (inputs.length != 2)
			throw new IllegalArgumentException(
					"More or less than 2 inputs have been given.");

		int totalLengthInputs = inputs[0].length() + inputs[1].length();
		if ((DISPLAY_WIDTH - totalLengthInputs) <= 0) {
            throw new MessageTooLongException("The 2 inputs are too long", DISPLAY_WIDTH);
		}
		
		String[] result = new String[DISPLAY_DEPTH];
		
		// Creating message
		System.arraycopy(splitString(message, DIALOG_MESSAGE_ROWS),
				0, result, 0, DIALOG_MESSAGE_ROWS);
		
		// Creating input buttons
		StringBuilder buttons = new StringBuilder();
		buttons.append(inputs[0]);
		for (int i = 0; i < DISPLAY_WIDTH - totalLengthInputs; i++)
			buttons.append(' ');
		
		buttons.append(inputs[1]);
		result[DISPLAY_DEPTH - 1] = buttons.toString();
		return result;
	}
	
	
	
	/**
	 * Helps placing strings. It should only be used for
	 * debugging/developing.  
	 * The display will look like that:  
	 * 
	 * 	1234567890123456
	 * 	1_______________
	 * 	2_______________
	 * 	3_______________
	 * 	4_______________
	 * 	5_______________
	 * 	6_______________
	 * 	7_______________
	 * 	8_______________
	 * 
	 * 
	 */
	public void showDebugDisplay() {
		StringBuilder firstRow = new StringBuilder();
		for (int i = 1; i <= DISPLAY_WIDTH; i++) {
			String number = new Integer(i).toString();
			firstRow.append(number.charAt(number.length() - 1));
		}
		
		String[] display = new String[DISPLAY_DEPTH];
		display[0] = firstRow.toString();
		for (int i = 2; i <= DISPLAY_DEPTH; i++) {
			StringBuilder row = new StringBuilder();
			row.append(i);
			row.append("_______________"); // 15 times _
			display[i - 1] = row.toString();
		}
		
		drawStringsOnDisplay(display);
	}
	
	/**
	 * Clears the row (i.e. removes inverted characters).
	 * 
	 * @param row the row to clear
	 * @return a clear row
	 */
	public static String clearRow(String row) {
		StringBuilder clearRow = new StringBuilder();
		int offset = 0;
		int lastInvertedCharPos;
		while ((lastInvertedCharPos = row.indexOf(INVERT_CHAR, offset)) > -1) {
			clearRow.append(row.substring(offset, lastInvertedCharPos));
			offset = lastInvertedCharPos + INVERT_CHAR.length();
		}
		clearRow.append(row.substring(offset, row.length()));
		
		return clearRow.toString();
	}
}
