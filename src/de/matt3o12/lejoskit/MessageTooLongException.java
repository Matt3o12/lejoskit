package de.matt3o12.lejoskit;

/**
 * This exception will be thrown if the message is too long
 * to be shown on the display. 
 * 
 * @author Matt3o12
 */
public class MessageTooLongException extends Exception {
	/**
	 * A {@link MessageTooLongException} with the message
	 * and the length the program expected.  
	 * Don't include a custom message as in a normal exception.
	 * Include the message that was too long.
	 * 
	 * For instance, if the message is "hello world" and the 
	 * length is 3, then the exception message
	 * ({@link Exception#getMessage()} will be:
	 * "The message: hello world is too long to be shown on the display.
	 * The maximum length is 3".
	 * 
	 * @param message The message that is too long
	 * @param maxLength The maximal length
	 */
	public MessageTooLongException(String message, int maxLength) {
		super("The message: " + message + " is too long to be shown "
				+ "on the display. The maximum length is "
				+ maxLength + ".");
	}
	
	/**
	 * A {@link MessageTooLongException} with the message that 
	 * is too long.   
	 * Don't include a custom message as in a normal exception.
	 * Include the message that was too long.
	 * 
	 * For instance, if the message is "hello world" then the
	 * exception message ({@link Exception#getMessage()} will be:
	 * "The message: hello world is too long to be shown on the display.".
	 * 
	 * @param message The message that is too long
	 */
	public MessageTooLongException(String message) {
		super("The message: " + message + " is too long to be shown on"
				+ "the display.");
	}
	
	/**
	 * A {@link MessageTooLongException} with the maximum length.    
	 * Don't include a custom message as in a normal exception.
	 * Include the message that was too long.
	 * 
	 * For instance, if the message is "hello world" and the 
	 * length is 3, then the exception message
	 * ({@link Exception#getMessage()} will be:
	 * "The message is too long to be shown on the display.
	 * The maximum length is 3".
	 * 
	 * @param maxLength The maximal length
	 */
	public MessageTooLongException(int maxLength) {
		super("The message is too long to be shown "
				+ "on the display. The maximum length is "
				+ maxLength + ".");
	}
	
	
}
