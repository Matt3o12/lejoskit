package de.matt3o12.lejoskit;

import de.matt3o12.lejoskit.config.Storable;
import lejos.nxt.Motor;
import lejos.robotics.RegulatedMotor;


/**
 * The human readable name of a motor port. 
 */
public enum MotorPortName implements Storable {
	/** Port A, id: 0, fullName: "Port A", shortName: 'A'. */
	PORT_A(0, "Port A", 'A'),

	/** Port B, id: 1, fullName: "Port B", shortName: 'B'. */
	PORT_B(1, "Port B", 'B'),
	
	/** Port C, id: 2, fullName: "Port C", shortName: 'C'. */
	PORT_C(2, "Port C", 'C');

	private int id;
	private String fullName;
	private char shortName;
	
	private MotorPortName(int id, String fullName, char shortName) {
		this.id = id;
		this.fullName = fullName;
		this.shortName = shortName;
	}

	/**
	 * Returns the port's id.  
	 * The IDs starts at 0 and range to 3.
	 *    
	 * @return the motor id.
	 */
	public int getID() {
		return id;
	}
	
	/**
	 * Returns the human readable name of the sensor port (like
	 * "Port 1").
	 * 
	 * @return the human readable name of the sensor.
	 */
	public String getFullName() {
		return fullName;
	}

	/**
	 * Returns the port identifier given by the NXT (A, B, C).
	 * 
	 * @return the port identifier (A, B, C).
	 */
	public char getShortName() {
		return shortName;
	}

	/**
	 * Returns the port name constant by the given name.  
	 * The name can be the ID (0 to 3), the ID, or the human
	 * readable name ("Port 1").  
	 * If the ID is an {@link Integer} use {@link #getPortByID(int)}
	 * instead.
	 * 
	 * @param name the name that should be used.
	 * @return the {@link MotorPortName} that belongs to the given name
	 */
	public static MotorPortName getPortByName(String name) {
		// If int return #getPortByID(int), else do nothing
		try {
			return getPortByID(Integer.parseInt(name));
		} catch (NumberFormatException e) {
			// do nothing since name isn't an integer.
		}
		
		if (name.length() == 1) {
			char shortName = name.charAt(0);
			for (MotorPortName portName : values()) {
				if (portName.getShortName() == shortName)
					return portName;
			}
		} else {
			for (MotorPortName port : values()) {
				if (port.name() == name || port.getFullName() == name)
					return port;
			}
		}
		
		throw new IllegalArgumentException("Invalid port name: '" + name + "'");
	}
	
	/**
	 * Returns the {@link MotorPortName} for the given {@link Integer}. If the
	 * ID is a string (that can be parsed to a number) you may also use 
	 * {@link #getPortByName(String)}.
	 * 
	 * @param id The ID of the port.
	 * @return The {@link MotorPortName}.
	 */
	public static MotorPortName getPortByID(int id) {
		switch (id) {
		case 0:
			return PORT_A;
			
		case 1:
			return PORT_B;
			
		case 2:
			return PORT_C;

		default:
			throw new IllegalArgumentException("No port was found for the given id: "
					+ id);
		}
	}
	
	/**
	 * Returns a {@link RegulatedMotor} for this motor port name. 
	 * @return a {@link RegulatedMotor} instance of the port representing the motor.
	 */
	public RegulatedMotor getMotor() {
		return Motor.getInstance(getID());
	}
	
	@Override
	public String getValue() {
		return Integer.toString(getID());
	}
}
