package de.matt3o12.lejoskit;

import java.util.ArrayList;
import java.util.List;

import lejos.nxt.Button;
import lejos.nxt.LCD;
import lejos.robotics.navigation.MoveController;
import de.matt3o12.lejoskit.config.Config;
import de.matt3o12.lejoskit.config.ConfigRunner;
import de.matt3o12.lejoskit.dialog.DialogManager;
import de.matt3o12.lejoskit.programs.CompletionEvent;
import de.matt3o12.lejoskit.programs.Program;
import de.matt3o12.lejoskit.programs.RoadToBridge;
import de.matt3o12.lejoskit.programs.ShowManual;

/**
 * A {@link Runner} runs multiple programs. It makes sure all programs
 * run in the given order, are cleared up properly, and it provides 
 * all programs with the config.
 */
public class Runner {
	private ArrayList<Program> programs;
	private DialogManager dialogManager;
	private Config config;

	/**
	 * Don't use this. The config can be obtained using
	 * {@link Config#getInstance()}.
	 * 
	 * Creates a new instance and adds the default programs if setup is true.
	 * *Warning*: the default programs may vary if you this from a subclass.  
	 * A custom {@link Config} can also be used. 
	 * 
	 * @param setup true if non-standart java classes
	 * 			should be set up (false is useful for unit testing)
	 * @param config the config instance to be used.
	 * @see #setup()
	 */
	@Deprecated
	public Runner(boolean setup, Config config) {
		programs = new ArrayList<Program>();
		if (setup)
			setup();
		
		this.config = config;
	}
	
	/**
	 * Creates a new instance and adds the default programs if setup is true.
	 * *Warning*: the default programs may vary if you this from a subclass.  
	 * The default {@link Config} instance will be used.
	 * 
	 * @param setup true if non-standart java classes
	 * 			should be set up (false is useful for unit testing)
	 * @see #Runner(boolean, Config)
	 */
	public Runner(boolean  setup) {
		this(setup, Config.getInstance());
	}
	
	/**
	 * Creates a new instance and adds the default programs. The default {@link Config}
	 * will be used.
	 * 
	 * @see #Runner(boolean)
	 */
	public Runner() {
		this(true);
	}
	
	/**
	 * Adds all recommend programs to the class.  
	 * It also adds a {@link DialogManager}.
	 */
	protected void setup() {
		dialogManager = new DialogManager();
	}

	/**
	 * Adds a program to be executed when start is called.
	 * 
	 * @param program The program to be added.
	 */
	public void addProgram(Program program) {
		programs.add(program);
	}
	
	/**
	 * Returns a list with all programs.
	 * 
	 * @return a list with all programs.
	 */
	public List<Program> getPrograms() {
		return programs;
	}
	
	/**
	 * Starts all programs that have been added so far.
	 * Take into account that programs might have been added
	 * by {@link #setup()}.
	 */
	public void start() {
		for (Program program : getPrograms()) {
			program.go(this);
			program.clearUp(this);
			
			CompletionEvent event = new CompletionEvent();
			program.onCompletion(event);
			
			if (event.isStop())
				break;
		}
	}

	/**
	 * @return the dialogManager
	 */
	public DialogManager getDialogManager() {
		return dialogManager;
	}

	/**
	 * @param dialogManager the dialogManager to set
	 */
	protected void setDialogManager(DialogManager dialogManager) {
		this.dialogManager = dialogManager;
	}
	
	/**
	 * @return the config
	 * @deprecated the config should be obtained using {@link Config#getInstance()}
	 */
	@Deprecated
	public Config getConfig() {
		return config;
	}
	
	/**
	 * Creates a {@link Runner} with the default programs.
	 * 
	 * @param args doesn't accept arguments.
	 * @throws Exception might be thrown by any method.
	 */
	@SuppressWarnings("checkstyle:magicnumber")
	public static void main(String[] args) throws Exception {
		Config c = Config.getInstance();
		c.setProperty("motor.left", MotorPortName.PORT_A.getValue());
		c.setProperty("motor.right", MotorPortName.PORT_B.getValue());
		c.setProperty("robot.trackwidth",
				String.valueOf(MoveController.WHEEL_SIZE_NXT2));
		c.setProperty("robot.wheeldiameter", "10");
		
		
		// Check if immediately done and remove arg if exists since 
		// ConfigRunner might block.
		boolean immediatelyDone = Runner.containsImmeidatelyDone(args, true);
		if (!Config.getInstance().couldLoadDefaultConfig())
			ConfigRunner.main(args);
		
		Runner run = new Runner();
		run.addProgram(new ShowManual());
		run.addProgram(new RoadToBridge());
		run.start();
		
		if (!immediatelyDone) {
			LCD.drawString("Technically done", 0, 7, true);
			Button.ESCAPE.waitForPressAndRelease();
		}
	}
	
	/**
	 * Looks for the `--immediatelyDone` arg in the args, removes the element
	 * if removeElement is true and returns true. If element wasn't found,
	 * it will return false.
	 * 
	 * TODO: Buggy.
	 * 
	 * @param args the args to search.
	 * @param removeElement true if the arg should be removed from the array
	 * 		(set to `null`).
	 * @return true if "--immediatelyDone" is in the array.
	 */
	public static boolean containsImmeidatelyDone(String[] args, boolean removeElement) {
		if (args == null)
			return false;
		
		for (int i = 0; i < args.length; i++) {
			String string = args[i];
			if ((string != null) && (string.equalsIgnoreCase("--immediatelyDone"))) {
				if (removeElement)
					args[0] = null;
				
				return true;
			}
		}
		
		return false;
	}
}
