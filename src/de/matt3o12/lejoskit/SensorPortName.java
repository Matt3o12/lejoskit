package de.matt3o12.lejoskit;

import de.matt3o12.lejoskit.config.Storable;
import lejos.nxt.SensorPort;

/**
 * The human readable name of a sensor port. 
 */
public enum SensorPortName implements Storable {
	/** Port number 1, id: 0, name: "Port 1". */
	PORT_1(0, "Port 1"),
	
	/** Port number 2, id: 1, name: "Port 2". */
	PORT_2(1, "Port 2"),
	
	/** Port number 3, id: 2, name: "Port 3". */
	PORT_3(2, "Port 3"),
	
	/** Port number 4, id: 3, name: "Port 4". */
	PORT_4(3, "Port 4");

	private int id;
	private String name;
	
	/**
	 * @param id the id defined by {@link SensorPort}.
	 * @param name a human readable name.
	 */
	private SensorPortName(int id, String name) {
		this.id = id;
		this.name = name;
	}

	/**
	 * Returns the port's id.  
	 * Warning port one doesn't have the ID 1. It actually has 
	 * 0, while the ID of port 4 is 3.  
	 * These IDs are defined by {@link SensorPort}.
	 *   
	 * @return the sensor id.
	 */
	public int getID() {
		return id;
	}
	
	/**
	 * Returns the human readable name of the sensor port (like
	 * "Port 1").
	 * 
	 * @return the human readable name of the sensor.
	 */
	public String getName() {
		return name;
	}

	/**
	 * Returns the port name constant by the given name.  
	 * The name can be the ID (0 to 3), or the human readable name
	 * ("Port 1").  
	 * If the ID is an {@link Integer} use {@link #getPortByID(int)}
	 * instead.
	 * 
	 * @param name the name of the port (e.g. "Port 1").
	 * @return the {@link SensorPortName} that belongs to the given name
	 */
	public static SensorPortName getPortByName(String name) {
		// If int return #getPortByID(int), else do nothing
		
		try {
			return getPortByID(Integer.parseInt(name));
		} catch (NumberFormatException e) {
			// do nothing since name isn't an integer.
		}
		
		for (SensorPortName port : values()) {
			if (port.name() == name || port.getName() == name)
				return port;
		}
				
		throw new IllegalArgumentException("Invalid port name: '" + name + "'");
	}
	
	/**
	 * Returns the {@link SensorPortName} for the given {@link Integer}. If the
	 * ID is a string (that can be parsed to a number) you may also use 
	 * {@link #getPortByName(String)}.
	 * 
	 * @param id The ID of the port.
	 * @return The {@link SensorPortName}.
	 */
	@SuppressWarnings("checkstyle:magicnumber")
	public static SensorPortName getPortByID(int id) {
		switch (id) {
		case 0:
			return PORT_1;
			
		case 1:
			return PORT_2;
			
		case 2:
			return PORT_3;
			
		case 3:
			return PORT_4;

		default:
			throw new IllegalArgumentException("No port was found for the given id: "
					+ id);
		}
	}
	
	/**
	 * Returns the Sensor Port for this port. 
	 * @return a {@link SensorPort} instance of the port representing the sensor.
	 */
	public SensorPort getSensorPort() {
		return SensorPort.getInstance(getID());
	}
	
	@Override
	public String getValue() {
		return Integer.toString(getID());
	}
}
