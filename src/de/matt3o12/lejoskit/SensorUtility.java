package de.matt3o12.lejoskit;

import lejos.nxt.LightSensor;

/**
 * Handy classes that make life easier with NXT's sensors.
 */
public final class SensorUtility {
	/**
	 * Utility classes should not have a public or default constructor.
	 */
	private SensorUtility() {
		
	}
	
	/**
	 * The light level boundary to determine if the floor is
	 * black when the light is on (default: 450).
	 */
	public static final int FLOOR_BLACK_BOUNDARY_LIGHT_ON = 450;
	
	/**
	 * The light level boundary to determine if the floor is
	 * black when the light is off (default: 200).
	 */
	public static final int FLOOR_BLACK_BOUNDARY_LIGHT_OFF = 200;
	
	/**
	 * Returns whether the ground is very dark (i.e. black).  
	 * This method calls {@link #isFloorBlack(LightSensor, int)} with the
	 * boundary value of 450 if the flood light is on or 200 if it is off.  
	 * **Warning:** the sensor is more accurate if the light is on. 
	 * 
	 * @param sensor the sensor to use
	 * @return true if the floor is black.
	 */
	public static boolean isFloorBlack(LightSensor sensor) {
		int boundary;
		if (sensor.isFloodlightOn())
			boundary = FLOOR_BLACK_BOUNDARY_LIGHT_ON;
		else
			boundary = FLOOR_BLACK_BOUNDARY_LIGHT_OFF;
		
		return isFloorBlack(sensor, boundary);
	}
	
	/**
	 * Returns whether the ground is very dark (i.e. black).  
	 * The boundary value can be changed. 145 is dark and 890
	 * sunlight. However, the real values are often higher or lower
	 * (keep in mind that if the light of the {@link LightSensor} is on,
	 * the value can be significantly higher). If you want 
	 * 
	 * @param sensor the sensor to use.
	 * @param boundaryValue the boundary value to use. 
	 * @return true if `sensor#getNormalizedLightValue() <= boundaryValue`
	 */
	public static boolean isFloorBlack(LightSensor sensor, int boundaryValue) {
		return sensor.getNormalizedLightValue() <= boundaryValue;
	}
}
