package de.matt3o12.lejoskit.config;

import de.matt3o12.lejoskit.Runner;
import de.matt3o12.lejoskit.dialog.SimpleDialog;
import de.matt3o12.lejoskit.dialog.YesNoDialog;
import de.matt3o12.lejoskit.dialog.YesNoDialogResult;
import de.matt3o12.lejoskit.programs.CompletionEvent;
import de.matt3o12.lejoskit.programs.SimpleDialogProgram;

/**
 * Creates a dialog that will ask if the user wants to update
 * the config. If the user selects no, the dialog will be 
 * {@link Runner} will be terminated.
 */
public class AskToSetConfig extends SimpleDialogProgram {
	private YesNoDialog dialog;
	
	/**
	 * Creates a new Dialog that will ask the user if he wants to update the config.
	 */
	public AskToSetConfig() {
		try {
			dialog =  new YesNoDialog("Config couldn't be load. Do you want"
					+ "to set the config manually?");
		} catch (Exception e) {
			System.out.println("Error creating dialog.");
			throw new RuntimeException(e);
		}
	}
	
	@Override
	public void onCompletion(CompletionEvent event) {
		super.onCompletion(event);
		
		/* If dialog class isn't YesNoDialog something went terrible wrong
		 * since it was created in the constructor.
		 * However, in case it isn't a SimpleDialog, just do nothing.
		 */
		if (getDialog() instanceof YesNoDialog) {
			YesNoDialog yesNoDialog = (YesNoDialog) getDialog();
			
			if (yesNoDialog.getYesNoResult() == YesNoDialogResult.NO)
				event.setStop(true);
		}
	}

	@Override
	public SimpleDialog getSimpleDialog() {
		return dialog;
	}
}
