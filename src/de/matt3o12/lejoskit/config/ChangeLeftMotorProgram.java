package de.matt3o12.lejoskit.config;

import de.matt3o12.lejoskit.DisplayUnit;

/**
 * Program to change the motor port on the left of the NXT.
 */
public class ChangeLeftMotorProgram extends ChangeMotorProgram {
	/**
	 * Creates a new {@link ChangeRightMotorProgram}.
	 */
	public ChangeLeftMotorProgram() {
		super();
	}

	@Override
	public String getConfigNode() {
		return "motor.left";
	}

	@Override
	public String[] getMessage() {
		String message = "Please select the port name for the left motor.";
		return new DisplayUnit().splitString(message);
	}
}
