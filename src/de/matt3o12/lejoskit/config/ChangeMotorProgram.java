package de.matt3o12.lejoskit.config;

import de.matt3o12.lejoskit.MotorPortName;

/**
 * Abstract class to change the motor of the nxt.  
 * The choices to make will be generated accordingly to 
 * {@link MotorPortName#values()}.
 */
public abstract class ChangeMotorProgram extends UpdateConfigProgram {
	/**
	 * Creates a new {@link ChangeMotorProgram}.
	 */
	public ChangeMotorProgram() {
		super();
	}
	
	@Override
	public PropertyChoice[] getChoices() {
		MotorPortName[] motors = MotorPortName.values();
		PropertyChoice[] choices = new PropertyChoice[motors.length];
		for (int i = 0; i < motors.length; i++) {
			MotorPortName motorPortName = motors[i];
			choices[i] = new PropertyChoice(motorPortName.getFullName(),
					motorPortName.getValue());
		}
		
		return choices;
	}
}
