package de.matt3o12.lejoskit.config;

import de.matt3o12.lejoskit.DisplayUnit;

/**
 * Prompts the dialog to change port of the motor on the right
 * of the device.
 */
public class ChangeRightMotorProgram extends ChangeMotorProgram {
	/**
	 * Creates a new {@link ChangeLeftMotorProgram}.
	 */
	public ChangeRightMotorProgram() {
		super();
	}

	@Override
	public String getConfigNode() {
		return "motor.right";
	}

	@Override
	public String[] getMessage() {
		String message = "Please select the port name for the right motor.";
		return new DisplayUnit().splitString(message);
	}
}
