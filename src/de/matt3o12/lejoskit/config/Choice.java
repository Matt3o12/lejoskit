package de.matt3o12.lejoskit.config;

import de.matt3o12.lejoskit.dialog.ChoiceDialog;

/**
 * A choice is what the item the user selects on a
 * {@link ChoiceDialog}.  
 * The choice just has a name but will always be unique (unless a
 * string), since it will always have its one hashcode.
 */
public class Choice {
	private String name;

	/**
	 * Creates a choice with the given name.
	 * 
	 * @param name the name to display.
	 */
	public Choice(String name) {
		setName(name);
	}
	
	/**
	 * Returns the name of the choice.
	 * 
	 * @return the name of the choice.
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name of the choice.
	 * 
	 * @param name the name to set.
	 */
	protected void setName(String name) {
		this.name = name;
	}
	
	@Override
	public String toString() {
		return "[Choice: " + getName() + "]";
	}
}
