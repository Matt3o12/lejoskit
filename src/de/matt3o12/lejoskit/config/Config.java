package de.matt3o12.lejoskit.config;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

import lejos.nxt.comm.RConsole;
import lejos.robotics.navigation.MoveController;
import de.matt3o12.lejoskit.CrashReporter;
import de.matt3o12.lejoskit.MotorPortName;
import de.matt3o12.lejoskit.SensorPortName;

/**
 * Stores all config values used for the NXT.   
 * It works exactly as Java's {@link Properties} does however,
 * it tries to load some default config values by default. It looks for
 * a file called exercise10.properties (TODO: change the name) on the NXT's
 * storage. In case it could be found, it will be used. If it couldn't be
 * found, the fallback config will be used. The fallback config is hardcoded
 * properties within the class (i.e. `fallbackProperties.setProperty(...)` is
 * used within the class). Unfortunately, there is no way to store resource files
 * within the NXJ (.jar) file.  
 * 
 * Config values required by LejosKit:
 * 
 * 	robot.wheeldiameter - The diameter of the wheels (default: {@link MoveController#WHEEL_SIZE_NXT1})
 * 	robot.trackwidth - The distance between the centers of the tracks of the two wheels.
 * 	motor.left - The motor on the left hand side (default: Port A).
 * 	motor.right - The motor on the right hand side (default: Port B).
 * 
 */
public class Config extends Properties {
	private File defaultFile = new File("exercise10.properties");
	
	/**
	 * Shutdown hook used to save the config. 
	 */
	private final class SaveConfigHook extends Thread {
		@Override
		public void run() {
			try {
				saveConfig();
			} catch (IOException e) {
				new CrashReporter(e).report();
			}
		}
	}
	
	private static Config instance;
	
	private static Properties fallbackProperties;
	private boolean couldLoadDefaultConfig;
	
	private void loadFallback() {
		// Since I can't load properties files wrapped into
		// the application's jar/nxj file, I need to set the
		// values manually.
		
		fallbackProperties = new Properties();
		fallbackProperties.setProperty("motor.left",
				MotorPortName.PORT_A.getValue());
		fallbackProperties.setProperty("motor.right",
				MotorPortName.PORT_B.getValue());
		
		// TODO: light sensor reworke. 
		fallbackProperties.setProperty("lightSensorLeft",
				SensorPortName.PORT_1.getValue());
		fallbackProperties.setProperty("lightSensorRight",
				SensorPortName.PORT_2.getValue());
		
		fallbackProperties.setProperty("robot.wheeldiameter",
				String.valueOf(MoveController.WHEEL_SIZE_NXT1));
	}
	
	/**
	 * Creates a new config and loads the default config values from the NXT if possible.
	 * 
	 * @deprecated use 
	 */
	@Deprecated
	public Config() {
		if (fallbackProperties == null)
			loadFallback();
		
		couldLoadDefaultConfig = false;
		if (defaultFile.exists()) {
			Properties defaultProperties = new Properties(fallbackProperties);
			try {
				defaultProperties.load(new FileInputStream(defaultFile));
			} catch (IOException e) {
				if (RConsole.isOpen()) {
					RConsole.println("Couldn't load properties. Exception was thrown");
					e.printStackTrace(RConsole.getPrintStream());					
				} else {
					System.out.println("Uncaught excpetion.");
				}
			}
			
			defaults = defaultProperties;
			couldLoadDefaultConfig = true;
		} else {
			defaults = fallbackProperties;
		}
		
		System.out.println(couldLoadDefaultConfig);
		Runtime.getRuntime().addShutdownHook(new SaveConfigHook());
	}

	/**
	 * Returns whether the default config could be loaded or not.  
	 * In case it couldn't the fallback config will be loaded. For more
	 * information about the config types, see {@link Config}.
	 * 
	 * @return true if the default config could be loaded
	 */
	public boolean couldLoadDefaultConfig() {
		return couldLoadDefaultConfig;
	}
	
	/**
	 * Returns the config instance to use. This class uses the singleton
	 * pattern, thus you should never call `new Config()`.  
	 * The constructor isn't private since {@link Config} hasn't always been
	 * a singleton pattern.
	 * 
	 * @return the config to use.
	 */
	public static Config getInstance() {
		if (instance == null)
			instance = new Config();
		
		return instance;
	}
	
	/**
	 * Puts a new property into this properties with key as key and storable as value.
	 * It basically calls {@link Properties#setProperty(String, String)} with 
	 * `Storable.getValue()` as the value. Thus, you will get what
	 * {@link Storable#getValue()} has returned when you call
	 * {@link #getProperty(String)} with the same key.
	 *   
	 * @param key the identifier (e.g. "motor.right").
	 * @param value the value to store.
	 */
	public synchronized void setProperty(String key, Storable value) {
		super.setProperty(key, value.getValue());
	}
	
	
	
	/**
	 * Saves the config.
	 * 
	 * @throws IOException if an error occurred while writing the file. 
	 */
	public synchronized void saveConfig() throws IOException {
		FileOutputStream os = new FileOutputStream(defaultFile);
		store(os, "Config saved on: " + System.currentTimeMillis());
		os.close();
	}
}
