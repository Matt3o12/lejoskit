package de.matt3o12.lejoskit.config;

import lejos.nxt.Button;
import lejos.nxt.LCD;
import de.matt3o12.lejoskit.Runner;

/**
 * Creates a lot of dialogs to configure the robot so that it can
 * run properly.
 */
public class ConfigRunner extends Runner {
	/**
	 * Creates a new {@link ConfigRunner} runner.
	 * 
	 * @param config the config to update.
	 */
	@SuppressWarnings("deprecation")
	public ConfigRunner(Config config) {
		super(true, config);
	}
	
	/**
	 * Creates a new {@link ConfigRunner}.
	 */
	public ConfigRunner() {
		this(Config.getInstance());
	}
	
	/**
	 * Starts a {@link ConfigRunner} that runs the configuration
	 * screen.
	 * 
	 * @param args no args are expected.
	 * @throws Exception any exception might be thrown.
	 */
	@SuppressWarnings("checkstyle:magicnumber")
	public static void main(String[] args) throws Exception {
		ConfigRunner runner = new ConfigRunner();
		runner.addProgram(new AskToSetConfig());	
		runner.addProgram(new ChangeLeftMotorProgram());
		runner.addProgram(new ChangeRightMotorProgram());
		
		
		runner.start();

		
		if (!Runner.containsImmeidatelyDone(args, false)) {
			LCD.drawString("Technically done", 0, 7, true);
			Button.ESCAPE.waitForPressAndRelease();			
		}
	}
}
