package de.matt3o12.lejoskit.config;

import java.util.Properties;

/**
 * A choice that can be stored into {@link Properties} when a
 * node (also known as key) is given.  
 * To write it into properties simply use:
 * 
 * 	PropertyChoice choice = new PropertyChoice("Hello", "hello");
 * 	Properties properties = new Properties();
 * 	properties.setProperty("myNode", choice.getValue());
 * 
 * Do not use name as the property value. The name is meant to be what
 * the user sees the choice is shown on the screen. {@link #getValue()}
 * is what should be used as the property value. Do not change the value
 * during development because the property might not work. 
 */
public class PropertyChoice extends Choice implements Storable {
	private String value;
	
	/**
	 * Creates a new {@link PropertyChoice} with the given name
	 * and value.
	 * 
	 * @param name the name to store.
	 * @param value the value to store.
	 */
	public PropertyChoice(String name, String value) {
		super(name);
		
		this.value = value;
	}
	
	/**
	 * Creates a new {@link PropertyChoice} with the given name
	 * and {@link Storable} object.
	 * 
	 * @param name the name to store.
	 * @param value the value to store.
	 */
	public PropertyChoice(String name, Storable value) {
		this(name, value.getValue());
	}
	
	/**
	 * Returns the value to store.
	 * 
	 * @return the value to store.
	 */
	public String getValue() {
		return value;
	}
}
