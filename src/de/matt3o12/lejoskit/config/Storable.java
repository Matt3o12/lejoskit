package de.matt3o12.lejoskit.config;

/**
 * Every class that extends this can be stored into properties.  
 * If you have a {@link Storable} object, and you want to store
 * it, call its `getValue` method to get the value.
 */
public interface Storable {
	/**
	 * Returns the value that should be stored into the properties.
	 * 
	 * @return the value to store.
	 */
	public String getValue();
}
