package de.matt3o12.lejoskit.config;

import de.matt3o12.lejoskit.dialog.ChoiceDialog;
import de.matt3o12.lejoskit.dialog.SimpleDialog;
import de.matt3o12.lejoskit.programs.CompletionEvent;
import de.matt3o12.lejoskit.programs.SimpleDialogProgram;

/**
 * Used to set specific values in the config. This class will
 * dynamically create a {@link ChoiceDialog} using {@link #getChoices()}
 * and {@link #getMessage()}. The result will be saved in the config.
 */
public abstract class UpdateConfigProgram extends SimpleDialogProgram {
	/** The dialog to use. */
	private ChoiceDialog dialog;
	
	/**
	 * Creates a new {@link SimpleDialogProgram}.
	 * 
	 * @param config used to save the result.
	 * @deprecated use {@link #UpdateConfigProgram()} instead.
	 */
	@Deprecated
	public UpdateConfigProgram(Config config) {
		
	}
	
	/**
	 * Creates a {@link UpdateConfigProgram}.
	 */
	public UpdateConfigProgram() {
		
	}
		
	@Override
	public SimpleDialog getSimpleDialog() {		
		return getChoiceDialog();
	}
	
	/** 
	 * You can overwrite this method to customize the way
	 * the choice dialog is created. 
	 * 
	 * @return returns the dialog that will be displayed.
	 */
	public ChoiceDialog getChoiceDialog() {
		if (dialog == null) {
			dialog = new ChoiceDialog() {
				@Override
				public String[] getMessage() {
					return UpdateConfigProgram.this.getMessage();
				}
				
				@Override
				public Choice[] getChoices() {
					return UpdateConfigProgram.this.getChoices();
				}
			};
		}
		
		return dialog;
	}
	
	@Override
	public void onCompletion(CompletionEvent event) {
		super.onCompletion(event);
		
		PropertyChoice choice = (PropertyChoice) getChoiceDialog().getSelectedChoice();
		Config.getInstance().setProperty(getConfigNode(), choice.getValue());
	}
	
	/**
	 * Returns the node where the result should be saved.  
	 * If you want to change the right motor, you will want
	 * to set the node to "motor.right". The result can be
	 * access by `config.getProperty("motor.right")`.
	 * 
	 * @return the node/key to save the config.
	 */
	public abstract String getConfigNode();
	
	/**
	 * Returns all choices to make.
	 * 
	 * @return all choices to make
	 */
	public abstract PropertyChoice[] getChoices();
	
	/**
	 * Returns the message that will appear on the screen.
	 * 
	 * @return the message on the screen.
	 */
	public abstract String[] getMessage();
}
