package de.matt3o12.lejoskit.dialog;

import de.matt3o12.lejoskit.DisplayUnit;
import de.matt3o12.lejoskit.config.Choice;

/**
 * A dialog with multiple choices. The dialog will display
 * the first choice on the very bottom of the screen. The
 * user can select the choices with the left or right arrow
 * button on the NXT.  
 * The selected choice can be obtained by {@link #getSelectedChoice()}.
 * 
 * You can define the choices by overwriting {@link #getChoices()}.
 */
public abstract class ChoiceDialog extends InputDialog {
	/** The string that should be displayed if there is a previous choice to select. */
	public static final String NEXT_INPUT = "  >";
	
	/** The string that should be displayed if there is a previous choice to select. */
	public static final String LAST_INPUT = "<  ";
	
	private int currentChoiceIndex = 0;
	
	@Override
	public String getInputRow() {
		String input = "\\i" + getChoices()[currentChoiceIndex].getName() + "\\i";
		String clearedInput = DisplayUnit.clearRow(input);
		// can the input be display like this: '<   input   >'.
		if (DisplayUnit.DISPLAY_WIDTH - clearedInput.length() 
				- NEXT_INPUT.length() - LAST_INPUT.length() > 0) {
			// invisible charaters are chars that won't be displayed
			// on the screen (e.g. \\i).
			int invisibleCharCount = input.length() - clearedInput.length();
			char[] row = new char[DisplayUnit.DISPLAY_WIDTH + invisibleCharCount];
			for (int i = 0; i < row.length; i++)
				row[i] = ' ';
			
			if (hasLessChoices())
				LAST_INPUT.getChars(0, LAST_INPUT.length(), row, 0);
			
			if (hasMoreChoices()) {
				int off = row.length - NEXT_INPUT.length();
				NEXT_INPUT.getChars(0, NEXT_INPUT.length(), row, off);
			}
			
			int centeredStringOff = getDisplayUnit().getCenterStringOffset(input);
			input.getChars(0, input.length(), row, centeredStringOff);
			return new String(row);
		} else {
			return getDisplayUnit().centerString(input);
		}
	}
	
	/**
	 * Returns the {@link Choice} that is currently selected.
	 * 
	 * @return the selected choice.
	 */
	public Choice getSelectedChoice() {
		return getChoices()[currentChoiceIndex];
	}
	
	/**
	 * All choices that the dialog knows. The choices shouldn't
	 * change when the dialog is displayed it may confuse the 
	 * program.
	 * 
	 * @return all choices to display
	 */
	public abstract Choice[] getChoices();
	
	/**
	 * @return the currentChoiceIndex
	 */
	public int getCurrentChoiceIndex() {
		return currentChoiceIndex;
	}

	/**
	 * @param currentChoiceIndex the currentChoiceIndex to set
	 */
	public void setCurrentChoiceIndex(int currentChoiceIndex) {
		this.currentChoiceIndex = currentChoiceIndex;
	}
	
	/**
	 * Returns if there is another choices for the user to select.
	 * For instance, when there are 3 choices, and the user has selected the
	 * first or second choice, this method will return true. If the
	 * user has selected the third choice, this method will return false
	 * 
	 * @return true if there are more choices.
	 */
	protected boolean hasMoreChoices() {
		return currentChoiceIndex + 1 < getChoices().length;
	}
	
	/**
	 * Returns true if there is at least one previous choices for the user to select.
	 * For instance, if the user has 3 choices, and the user has selected the
	 * second or third one, this method will return true. If the user has 
	 * selected the first one, this method will return false.
	 * 
	 * @return true, if there aren't less choices.
	 */
	protected boolean hasLessChoices() {
		return currentChoiceIndex - 1 >= 0;
	}

	@Override
	public void onCancelPress() {
		super.onCancelPress();
		invalidInput();
	}
	
	@Override
	public void onLeftPress() {
		super.onLeftPress();
		
		if (hasLessChoices()) {
			currentChoiceIndex -= 1;
			validInput();
		} else {
			invalidInput();
		}
	}
	
	@Override
	public void onRightPress() {
		super.onRightPress();
		
		if (hasMoreChoices()) {
			currentChoiceIndex += 1;
			validInput();
		} else {
			invalidInput();
		}
	}
	
	@Override
	public void onOkPress() {
		super.onOkPress();
		setDone();
	}
}
