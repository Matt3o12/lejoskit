package de.matt3o12.lejoskit.dialog;

/**
 * A dialog is a "long message". This message can either expect the user to
 * hit enter and don't show any input buttons or except him to choose from
 * several options.  
 */
public interface Dialog {
	/**
	 * Shows the dialog the user. This is the part where the complete dialog
	 * should be drawn to the display.
	 */
	public void showDialog();

	/**
	 * Returns whether the dialog is done (the user make his choice).
	 * @return the done
	 */
	public boolean isDone();
	
	/**
	 * Sets that the dialog is done.  
	 * If you implement this method, make sure it can't be change to 
	 * false since once a method is done, it can't be undone. Further 
	 * steps such as cleaning the display should be done here.
	 */
	public void setDone();
}
