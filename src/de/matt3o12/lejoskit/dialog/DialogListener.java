package de.matt3o12.lejoskit.dialog;

/**
 * The listener interface. Its method are called when a button is pressed.
 * It needs to be registered along with a {@link Dialog} instance at the
 * {@link DialogListener}  
 * It is common to combine the {@link Dialog} and the {@link DialogListener}
 * within one calls when creating a custom dialog.  
 * If you're looking for a simple way to create a dialog, check out
 * {@link SimpleDialog} and its subclasses.  
 * 
 * The bottom and top button are normally associated with a cancel button and
 * an OK button.  
 */
public interface DialogListener {
	/**
	 * Called when the right button has been pressed.
	 */
	public void onRightPress();	
	
	/**
	 * Called when the left button has been pressed.
	 */
	public void onLeftPress();
	
	/**
	 * Called when the cancel (bottom) button is pressed.
	 */
	public void onCancelPress();
	
	/**
	 * Called when the OK button is pressed.
	 */
	public void onOkPress();
}	
