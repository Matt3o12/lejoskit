package de.matt3o12.lejoskit.dialog;

import java.util.ArrayList;
import java.util.List;

import lejos.nxt.Button;
import lejos.nxt.ButtonListener;
import de.matt3o12.lejoskit.Runner;
import de.matt3o12.lejoskit.programs.Program;

/**
 * The {@link DialogManager} manages what dialog should be displayed. It also
 * makes sure that a {@link Dialog} will be removed when the {@link Program}
 * doesn't need it anymore (it needs help from {@link Runner} of course).
 * 
 * This class isn't finished yet and may not work as expected!
 */
public class DialogManager implements ButtonListener {
	private ArrayList<DialogListener> dialogListeners;
	
	/**
	 * Creates a new {@link DialogManager} that listens to every
	 * Button event.
	 */
	public DialogManager() {
		dialogListeners = new ArrayList<DialogListener>();
		addButtonListeners();
	}
	
	/**
	 * Adds this class to all button listeners.
	 */
	protected void addButtonListeners() {
		Button.LEFT.addButtonListener(this);
		Button.RIGHT.addButtonListener(this);
		Button.ESCAPE.addButtonListener(this);
		Button.ENTER.addButtonListener(this);
	}
	
	/**
	 * Tells the best matching listener what {@link Button} b has been pressed.
	 * 
	 * Warning: doesn't work right now. It calls every listener. TODO
	 * 
	 * @param b The button has been pressed.
	 */
	public void buttonEvent(Button b) {
		if (dialogListeners == null)
			return;
		
		// TODO: priority support.
		
		for (DialogListener theListener : dialogListeners) {
			switch (b.getId()) {
			case Button.ID_ENTER:
				theListener.onOkPress();
				break;

			case Button.ID_ESCAPE:
				theListener.onCancelPress();
				break;

			case Button.ID_LEFT:
				theListener.onLeftPress();
				break;

			case Button.ID_RIGHT:
				theListener.onRightPress();
				break;
				
			default:
				break;
			}
		}
	}
	
	/**
	 * Adds a listener to the dialog manager.
	 * 
	 * @param listener the listener to add
	 */
	public void addListener(DialogListener listener) {
		dialogListeners.add(listener);
	}
	
	/**
	 * Removes a listener that was added before.
	 * 
	 * @param listener The listener to be removed.
	 * @return true if this list contained the specified element.
	 */
	public boolean removeListener(DialogListener listener) {
		return dialogListeners.remove(listener);
	}
	
	/**
	 * Returns all listeners that are called when the user presses a button.
	 * 
	 * @return all listeners to call when a button is pressed.
	 */
	public List<DialogListener> getListeners() {
		return dialogListeners;
	}
		
	/**
	 * Called to "end" the listener. It won't response to any button events anymore.
	 */
	public void end() {
		dialogListeners = null;
	}
	
	@Override
	public void buttonReleased(Button b) {
		
	}
	
	
	@Override public void buttonPressed(Button b) {
		buttonEvent(b);
	}
}
