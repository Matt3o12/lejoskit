package de.matt3o12.lejoskit.dialog;

import de.matt3o12.lejoskit.DisplayUnit;

/**
 * Shows a dialog that displays a message over the first 5 rows
 * and one or more input options at the very bottom of the display.  
 * You can set the messages by overwriting {@link #getInputRow()} and
 * {@link #getMessage()}. You also need to tell the Dialog when it
 * should be done and when the command was invalid.  
 * You can best check that by overwriting the the listener methods. Than
 * simply call {@link #setDone()} if the input was right one or {@link #invalidInput()}
 * if the input was invalid.  
 */
public abstract class InputDialog extends SimpleDialog {
	/** The maximal length of the message in a {@link YesNoDialog}. */
	public static final int MAX_MESSAGE_LENGTH = 5;
		
	/** How long should the command be marked as invalid. */
	public static final long INVALID_COMMAND_SHOW_TIME = 3000L;
	
	/** The row where the invalid input message should be displayed. */
	public static final int INVALID_INPUT_ROW = 3;
	
	private DisplayUnit displayUnit;
	
	private String invalidInputMessage;
	
	/** When was the command mark as invalid */
	private long invalidCommand;

	/**
	 * Creates an InputDialog.
	 */
	public InputDialog() {
		displayUnit = new DisplayUnit();
		invalidInputMessage = displayUnit.centerString("\\iInvalid input\\i");
	}
	
	/**
	 * Returns the display unit that this method uses.
	 * TODO: make {@link DisplayUnit} completely static.
	 * 
	 * @return the {@link DisplayUnit} to use in this class
	 */
	protected DisplayUnit getDisplayUnit() {
		return displayUnit;
	}

	
	@Override
	public String[] getRows() {
		String[] rows = new String[DisplayUnit.DISPLAY_DEPTH];
		if (shouldHideInvalidInputMessage()) {
			System.arraycopy(getMessage(), 0, rows, 0, MAX_MESSAGE_LENGTH);
		} else {
			rows[INVALID_INPUT_ROW] = getInvalidInputRow();
		}
		
		// Display the input on the last row
		rows[DisplayUnit.DISPLAY_DEPTH - 1] = getInputRow();
		return rows;
	}
	
	/**
	 * Returns whether the invalid input message should be hidden or not.
	 * 
	 * @return true if the input message should be hidden.
	 */
	public boolean shouldHideInvalidInputMessage() {
		return (System.currentTimeMillis() - (INVALID_COMMAND_SHOW_TIME
				+ invalidCommand)) > 0;
	}
	
	/**
	 * Sets that the input was incorrect. It will cause {@link #getInputRow()} to
	 * display for {@value #INVALID_COMMAND_SHOW_TIME} milliseconds.  
	 */
	public void invalidInput() {
		invalidCommand = System.currentTimeMillis();
	}
	
	/**
	 * Opposite of {@link #invalidInput()}. It makes the invalid input message vanish.
	 * You should only call it when the user corrected his input but the dialog
	 * isn't finished.
	 */
	public void validInput() {
		invalidCommand = 0;
	}
	
	/**
	 * Returns the row that should be display when the input was invalid.  
	 * The default value is 'Invalid input' (centered). Make sure that
	 * the string is long enough to be displayed.
	 * 
	 * This method is frequently called.  
	 * Make sure you cache the value so that it isn't calculated each time
	 * the method is called. 
	 * 
	 * @return string that tells the user that the input was invalid. 
	 */
	public String getInvalidInputRow() {
		return invalidInputMessage;
	}
	
	/**
	 * Returns the row that shows all possible input option. It should be
	 * something like 'ok?' or 'yes		no'.
	 * 
	 * This method is frequently called.  
	 * Make sure you cache the value so that it isn't calculated each time the
	 * method is called. 
	 * 
	 * @return all input options as a string.
	 */
	public abstract String getInputRow();
	
	/**
	 * Returns the message that should be displayed. The array needs to be 5
	 * or longer otherwise an {@link ArrayIndexOutOfBoundsException} will be
	 * called. If the array is longer than 5, only the first 5 lines will be
	 * used.
	 *  
	 * This method is frequently called.  
	 * Make sure you cache the value so that it isn't calculated each time
	 * the method is called. 
	 * 
	 * @return the message to show.
	 */
	public abstract String[] getMessage();
}
