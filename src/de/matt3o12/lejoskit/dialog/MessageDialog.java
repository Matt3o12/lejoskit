package de.matt3o12.lejoskit.dialog;

import de.matt3o12.lejoskit.DisplayUnit;
import de.matt3o12.lejoskit.MessageTooLongException;

/**
 * Displays a dialog that only has one message
 * (over all rows). It doesn't have input buttons.  
 * To tell the program, "Ok, I got that", simply press
 * the Ok (top) button.
 */
public class MessageDialog extends SimpleDialog {
	private String[] message;
	
	/**
	 * Creates a new Message dialog with the given message.
	 * The message mustn't be longer than 16.
	 * 
	 * @param message the message to display on the screen.
	 * @throws MessageTooLongException if message is longer than 8.
	 */
	public MessageDialog(String message) throws MessageTooLongException {
		try {
			this.message = new DisplayUnit().splitString(message);
		} catch (ArrayIndexOutOfBoundsException e) {
			throw new MessageTooLongException(message, DisplayUnit.DISPLAY_DEPTH);
		}
	}

	@Override
	public String[] getRows() {
		return message;
	}
	
	@Override
	public void onOkPress() {
		super.onOkPress();
		setDone();
	}
}
