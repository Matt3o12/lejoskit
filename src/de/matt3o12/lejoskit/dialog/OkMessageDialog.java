package de.matt3o12.lejoskit.dialog;

import de.matt3o12.lejoskit.MessageTooLongException;

/**
 * An {@link OkMessageDialog} is a dialog that show the given message
 * (the message can't take more than 5 rows or an exception will be thrown)
 * and it also shows "Ok" on the very bottom of the screen (row 8).  
 * The message is centered and if the user presses another button than Enter,
 * the message will vanish and "Invalid command" (centered) will be displayed
 * on the 3rd row of the display. After 3 seconds (might take longer when the
 * display isn't refreshed), the default message will appear again.
 */
public class OkMessageDialog extends InputDialog {
	/** The maximal length of the message in a {@link YesNoDialog}. */
	public static final int MAX_MESSAGE_LENGTH = 5;
	
	private String[] message;
	private String inputRow;
	
	/**
	 * Creates an {@link OkMessageDialog} with the message.  
	 * The message can't take more than 5 rows or the 
	 * {@link MessageTooLongException} will be thrown.
	 * 
	 * @param message The message to show
	 * @throws MessageTooLongException thrown if the message took more than 5 rows. 
	 */
	public OkMessageDialog(String message) throws MessageTooLongException {
		super();
		
		try {
			this.message = getDisplayUnit().splitString(message, MAX_MESSAGE_LENGTH);
		} catch (ArrayIndexOutOfBoundsException e) {
			throw new MessageTooLongException(message, MAX_MESSAGE_LENGTH);
		}
		
		inputRow = getDisplayUnit().centerString("Ok");
	}

	@Override
	public String getInputRow() {
		return inputRow;
	}

	@Override
	public String[] getMessage() {
		return message;
	}
	
	@Override
	public void onOkPress() {
		super.onOkPress();
		setDone();
	}
	
	@Override
	public void onCancelPress() {
		super.onCancelPress();
		invalidInput();
	}
	
	@Override
	public void onLeftPress() {
		super.onLeftPress();
		invalidInput();
	}
	
	@Override
	public void onRightPress() {
		super.onRightPress();
		invalidInput();
	}
}
