package de.matt3o12.lejoskit.dialog;

import lejos.nxt.LCD;
import de.matt3o12.lejoskit.DisplayUnit;
import de.matt3o12.lejoskit.programs.DialogProgram;

/**
 * The subclass that display a simple dialog. A simple dialog
 * may only have characters. These characters should be split
 * into rows. The dialog must not have more than 8 rows. Each
 * row must not have more than 16 characters. The rows need to
 * be returned by {@link #getRows()}. When the user presses a
 * button, on...Press will be called. It sets the result to 
 * the button that was pressed. You will need to overwrite at
 * least on of these listener methods and call {@link #setDone()}
 * in it (don't forget to tell its super method when overwriting).
 * It will tell the dialog that "it is done and should stop displaying
 * the dialog". It it necessary for {@link DialogProgram} to complete. 
 */
public abstract class SimpleDialog implements Dialog, DialogListener {
	private SimpleDialogResult result;
	private boolean done;
	
	/**
	 * This method returns all rows that should be displayed. The length of
	 * the String array shouldn't be longer than 8.  
	 * The method is called every time a method needs to show its rows. It 
	 * can be called multiple times (even more than once per second in a regular
	 * interval). So make sure this method doesn't do heavy tasks like splitting
	 * a string. These things should be cached or already calculated. Class a field
	 * is a good example for that.
	 * 
	 * @return All rows to display.
	 */
	public abstract String[] getRows();
		
	@Override
	public void showDialog() {
		new DisplayUnit().drawStringsOnDisplay(getRows());
	}
	
	
	/**
	 * Gets the result of the current dialog.  
	 * Warning: make sure the dialog {@link #isDone()} before
	 * you use this result. 
	 * 
	 * @return the result
	 */
	public SimpleDialogResult getResult() {
		return result;
	}

	/**
	 * Changes the result.  
	 * If you want to check when the user clicks something, you should
	 * rather use one of the listeners.
	 * 
	 * @param result the result to set
	 */
	protected void setResult(SimpleDialogResult result) {
		this.result = result;
	}
	
	@Override
	public boolean isDone() {
		return done;
	}

	/**
	 * Sets that the dialog is done. Once finished, it can't be changed.  
	 * It also cleans the LCD display.
	 */
	@Override
	public void setDone() {
		this.done = true;
		LCD.clear();
	}
	
	@Override
	public void onRightPress() {
		this.setResult(SimpleDialogResult.RIGHT_INPUT);
	}	
	
	@Override
	public void onLeftPress() {
		this.setResult(SimpleDialogResult.LEFT_INPUT);
	}
	
	@Override
	public void onCancelPress() {
		this.setResult(SimpleDialogResult.CANCEL);
	}
	
	@Override
	public void onOkPress() {
		this.setResult(SimpleDialogResult.OK);
	}
}
