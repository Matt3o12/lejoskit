package de.matt3o12.lejoskit.dialog;

/**
 * A simple result a dialog can have.  
 * Since you can type a text with the buttons, it can only have
 * 4 results. It should mostly be used but feel free to use your
 * own enum with results.
 */
public enum SimpleDialogResult {
	/**
	 * Left button was pressed.
	 */
	LEFT_INPUT,
	
	/**
	 * The right button was pressed.
	 */
	RIGHT_INPUT, 
	
	/**
	 * The top button was pressed.  
	 * Normally associated with Ok.
	 */
	OK,
	
	/**
	 * The cancel button was pressed.  
	 * Normally associated stop, whatever you're doing or
	 * no I don't want that.
	 */
	CANCEL
}
