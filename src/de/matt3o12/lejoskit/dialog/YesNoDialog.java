package de.matt3o12.lejoskit.dialog;

import de.matt3o12.lejoskit.DisplayUnit;
import de.matt3o12.lejoskit.MessageTooLongException;

/**
 * A dialog that displays a message with "yes" and "no" as inputs.  
 * The left button on the NXT is associated with yes and the right one
 */
public class YesNoDialog extends InputDialog {
	private YesNoDialogResult result;

	/**
	 * The allowed number of characters for both
	 * inputs. 
	 */
	public static final int MAX_INPUT_LENGTH_TOTAL = 11;
	
	private String[] message;
	private String inputRow;
	
	/**
	 * Creates a dialog with "Yes" and "No" as inputs and a message. The message
	 * may not be longer than 11 or a
	 * {@link MessageTooLongException} will be thrown.
	 * 
	 * @param message the message to display on the top
	 * @throws MessageTooLongException thrown if param message is too long.
	 */
	public YesNoDialog(String message) throws MessageTooLongException {
		super();
		
		try {
			this.message = getDisplayUnit().splitString(message, MAX_MESSAGE_LENGTH);
		} catch (ArrayIndexOutOfBoundsException e) {
			throw new MessageTooLongException(message, MAX_MESSAGE_LENGTH);
		}
		
		inputRow = createInputRow("Yes", "No");
	}
	
	
	/**
	 * Creates the input rows.
	 * For example, if the inputs are input1, and tow the result
	 * will look like that (without '|' on the sides and '_' on the top): 
	 *  _________________ 
	 *  |input1   input2|
	 * 
	 * @param input1
	 *            The left input
	 *            
	 * @param input2
	 * 			  The right input.
	 *            
	 * @return A row with the first input to the left, and the second one to the
	 *         right.
	 * @throws MessageTooLongException
	 *             Thrown if the 2 inputs can be displayed in one row.    
	 */
	public String createInputRow(String input1, String input2)
			throws MessageTooLongException {

		int totalLengthInputs = input1.length() + input1.length();
		if ((DisplayUnit.DISPLAY_WIDTH - totalLengthInputs) <= 0) {
			throw new MessageTooLongException("The 2 inputs are too long",
					MAX_INPUT_LENGTH_TOTAL);
		}
				
		StringBuilder buttons = new StringBuilder();
		buttons.append(input1);
		for (int i = 0; i < DisplayUnit.DISPLAY_WIDTH - totalLengthInputs; i++)
			buttons.append(' ');
		
		buttons.append(input2);
		return buttons.toString();
	}
	
	@Override
	public String getInputRow() {
		return inputRow;
	}

	@Override
	public String[] getMessage() {
		return message;
	}
	
	@Override
	public void onLeftPress() {
		super.onLeftPress();
		setYesNoResult(YesNoDialogResult.YES);
		setDone();
	}
	
	@Override
	public void onRightPress() {
		super.onRightPress();
		
		setYesNoResult(YesNoDialogResult.NO);
		setDone();
	}
	
	@Override
	public void onOkPress() {
		super.onOkPress();
		invalidInput();
	}
	
	@Override
	public void onCancelPress() {
		super.onCancelPress();
		invalidInput();
	}
	
	@Override
	public void invalidInput() {
		super.invalidInput();
		
		setYesNoResult(YesNoDialogResult.NON);
	}
	
	/**
	 * Sets the dialog result. Should only be called from a listener.
	 * 
	 * @param result the result the user entered.
	 */
	protected void setYesNoResult(YesNoDialogResult result) {
		this.result = result;
	}
	
	/**
	 * Returns the result (did the user press yes or no).
	 * 
	 * @return the result telling what the used did.
	 */
	public YesNoDialogResult getYesNoResult() {
		return result;
	}
}
