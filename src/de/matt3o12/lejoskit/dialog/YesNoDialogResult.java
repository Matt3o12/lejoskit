package de.matt3o12.lejoskit.dialog;

/**
 * The result of a {@link YesNoDialog}.  
 * When the dialog it will either be {@link #YES} or {@link #NO}. In case it is
 * {@link #NON}, something is broken. It can only be {@link #NON} when an invalid
 * button was pressed. 
 */
public enum YesNoDialogResult {
	/** The button associated with yes. */
	YES,
	
	/** The button associated with no. */
	NO,
	
	/** An invalid button was pressed. The dialog should be finished. */
	NON;
}
