package de.matt3o12.lejoskit.programs;

import de.matt3o12.lejoskit.Runner;

/**
 * An event that tells a {@link Runner} what to do next.  
 * For the time being, this method can only tell the
 * {@link Runner} to stop but I plan on expending it to 
 * tell a specific {@link Program} to run and maybe even more. 
 */
public class CompletionEvent {
	private boolean stop;
	
	/**
	 * Returns if the whole {@link Runner} should stop.
	 * 
	 * @return true if it should stop.
	 */
	public boolean isStop() {
		return stop;
	}

	/**
	 * Sets if the whole runner should stop after. 
	 * 
	 * @param stop true if the {@link Runner} should stop.
	 */
	public void setStop(boolean stop) {
		this.stop = stop;
	}
}
