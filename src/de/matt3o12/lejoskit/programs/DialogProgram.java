package de.matt3o12.lejoskit.programs;

import lejos.nxt.LCD;
import de.matt3o12.lejoskit.Runner;
import de.matt3o12.lejoskit.dialog.Dialog;
import de.matt3o12.lejoskit.dialog.DialogListener;

/**
 * A program that only displays a dialog.
 */
public abstract class DialogProgram extends Program {
	private static final long WAIT_TIME_FOR_USER = 300L;
	private Dialog dialog;
	
	/**
	 * Returns the dialog. This method is called once. This
	 * {@link Program} will use the this dialog all the time.
	 * 
	 * @return the dialog to show
	 */
	public abstract Dialog getDialog();

	/**
	 * Returns the DialogListener. Make sure that this method doesn't
	 * return another dialog each time that it is called. Cache the 
	 * dialog somewhere!
	 * 
	 * @return the dialogListener to show.
	 */
	public abstract DialogListener getDialogListener();
	
	@Override
	public void go(Runner runInstance) {
		if (dialog == null)
			dialog = getDialog();
		
		runInstance.getDialogManager().addListener(getDialogListener());
		dialog.showDialog();
		waitForUser();
	}
	
	/**
	 * Waits until the dialog has finished. Reloads the dialog every 
	 * 300 milliseconds. (i.e. {@link Dialog#showDialog()} is called).
	 * In the meantime it will call {@link Thread#sleep(long)}.
	 * 
	 * If the current {@link Thread} is Interrupted, it will just return.  
	 * {@link Dialog#showDialog()} will be called before the current 
	 * {@link Thread} sleeps.
	 */
	public void waitForUser() {
		while (!getDialog().isDone()) {
			getDialog().showDialog();
			try {
				Thread.sleep(WAIT_TIME_FOR_USER);
			} catch (InterruptedException e) {
				// Fixes "Must have at least one statement" warning.
				continue;
			}

		}	
	}
	
	@Override
	public void clearUp(Runner runInstance) {
		super.clearUp(runInstance);
		runInstance.getDialogManager().removeListener(getDialogListener());
		LCD.clear();
	}
}
