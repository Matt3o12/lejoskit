package de.matt3o12.lejoskit.programs;

import lejos.robotics.RegulatedMotor;
import lejos.robotics.navigation.DifferentialPilot;
import lejos.robotics.navigation.MoveController;
import lejos.robotics.navigation.RotateMoveController;
import de.matt3o12.lejoskit.MotorPortName;
import de.matt3o12.lejoskit.Runner;
import de.matt3o12.lejoskit.config.Config;

/**
 * With this program you can make the robot move using a {@link RotateMoveController}.  
 * You will only need to overwrite {@link #move(RotateMoveController)} to do so. The
 * move controller will already be given using the default settings.   
 * If you want to use your custom {@link RotateMoveController} overwrite
 * {@link #getMoveController(RegulatedMotor, RegulatedMotor)}
 */
public abstract class MoveControllerProgram extends Program {
	private RotateMoveController pilot;
	
	/**
	 * Returns the move controller. If you want to provide a custom
	 * {@link MoveController}, overwrite this method.  
	 * 
	 * @param leftMotor the left {@link RegulatedMotor} to use.
	 * @param rightMotor the right {@link RegulatedMotor} to use. 
	 * @return a {@link DifferentialPilot} with the default settings of this class.
	 */
	public RotateMoveController getMoveController(RegulatedMotor leftMotor,
			RegulatedMotor rightMotor) {
		// TODO: Check if left and/or right motor have change.
		if (pilot == null) 
			pilot = new DifferentialPilot(
					getWheelDiameter(), getTrackWidth(), leftMotor, rightMotor
			);
		
		return pilot;
	}

	@Override
	public void go(Runner runInstance) {
		Config c = Config.getInstance();
		
		RegulatedMotor left = getMotorFromConfig(c, "motor.left");
		RegulatedMotor right = getMotorFromConfig(c, "motor.right");

		RotateMoveController moveController = getMoveController(left, right);
		move(moveController);
	}
	
	private RegulatedMotor getMotorFromConfig(Config c, String node) {
		return MotorPortName.getPortByName(c.getProperty(node)).getMotor();
	}
	
	/**
	 * Here is the code that makes the robot move. When this method returns
	 * the robot supposes that this program has ended (i.e. everything will
	 * be closed).  
	 * Make sure you call {@link #shouldStop()} frequently to make sure the
	 * robot stops when it is told to, even though you might not have
	 * overwritten it. A subprogram might have. However, make also sure the robot
	 * is in a good state to stop (it should neither be moving, nor doing something
	 * where it shouldn't stop). 
	 *  
	 * @param moveController the {@link RotateMoveController}.
	 */
	public abstract void move(RotateMoveController moveController);
	@Override
	public void clearUp(Runner runInstance) {
		super.clearUp(runInstance);
		
		// the move controller should already have been set.
		// So null will be fine. TODO: improve with getMoveController
		getMoveController(null, null).stop();
	}
	
	/**
	 * This method can be overwritten to customize the point when the robot
	 * ends. Sometimes the robot might need to drive as long as a certain event
	 * occurs (like a touch) and if that happens, the path should be completed,
	 * even if the last way point isn't reached.  
	 * If you want a way point that doesn't end, set at least coordinate of
	 * the way point to {@link Double#POSITIVE_INFINITY} or
	 * {@link Double#NEGATIVE_INFINITY}
	 * 
	 * This method will always return `false` by default. This method might frequently
	 * be called. So, make sure it won't do any heavy calculations that slow the robot
	 * down.  
	 * 
	 * @return if the robot should stop. 
	 */
	public boolean shouldStop() {
		return false;
	}

	/**
	 * Returns the Diameter of the tire in mm. You can overwrite this
	 * method to customize the wheel diameter value.
	 * 
	 * @return the wheel diameter
	 */
	public float getWheelDiameter() {
		return Float.parseFloat(
				Config.getInstance().getProperty("robot.wheeldiameter"));
	}

	/**
	 * Returns the distance between the two wheels in mm to use for
	 * this controller. You can overwrite this method to customize
	 * the track width value.
	 * 
	 * @return the track width
	 */
	public float getTrackWidth() {
		return Float.parseFloat(Config.getInstance().getProperty("robot.trackwidth"));
	}
}
