package de.matt3o12.lejoskit.programs;

import lejos.robotics.navigation.Navigator;
import lejos.robotics.navigation.RotateMoveController;
import lejos.robotics.navigation.Waypoint;
import lejos.robotics.pathfinding.Path;
import de.matt3o12.lejoskit.Runner;

/**
 * A {@link Program} using {@link Navigator} to navigate to a given {@link Path}.
 */
public abstract class NaviProgram extends MoveControllerProgram {
	/**
	 * The default time the robot should sleep before checking if
	 * the robot is long. The longer the time is, the less CPU will
	 * be used, however, the robot acts slowly and might miss some
	 * lines. The longer the time is the more CPU will be used and
	 * the robot will have less battery.  
	 */
	private static final long MOVING_SLEEP_TIME = 100L;
	
	private Navigator navi;
	
	/**
	 * Return the path that the robot should drive.
	 * 
	 * @return The path to drive.
	 */
	public abstract Path getPath();	
		
	/**
	 * If you want to customize the {@link Navigator}, overwrite this method.  
	 * If you simply want to get the navi that is used, call {@link #getNavi()}. 
	 * 
	 * This method is called every time {@link #move(RotateMoveController)}
	 * is called (i.e. every time, the robot is set to run).
	 * 
	 * @param moveController a {@link RotateMoveController} for controlling the robot.
	 * @return the navigator.
	 */
	public Navigator createNavi(RotateMoveController moveController) {
		navi = new Navigator(moveController);
		return navi;
	}
	
	/**
	 * Returns the current navi.  
	 * If you want to customize the {@link Navigator}, overwrite
	 * {@link #createNavi(RotateMoveController)}.
	 * 
	 * @param moveController a {@link RotateMoveController} for controlling the robot.
	 * @return the navigator.
	 */
	public Navigator getNavi() {
		return navi;
	}
	
	/**
	 * Creates a {@link Waypoint}. Shortcut for `new Waypoint(x, y)`
	 * 
	 * @param x the x coordinate
	 * @param y the y coordinate
	 * @return a {@link Waypoint} instance.
	 */
	public Waypoint createWaypoint(int x, int y) {
		return createWaypoint((double) x, (double) y);
	}
	
	/**
	 * Creates a {@link Waypoint}. Shortcut for `new Waypoint(x, y)`
	 * 
	 * @param x the x coordinate
	 * @param y the y coordinate
	 * @return a {@link Waypoint} instance.
	 */
	public Waypoint createWaypoint(double x, double y) {
		return new Waypoint(x, y);
	}

	@Override
	public void move(RotateMoveController moveController) {
		Navigator navi = createNavi(moveController);
		navi.followPath(getPath());
		
		while (navi.isMoving() && !shouldStop()) {
			try {
				Thread.sleep(MOVING_SLEEP_TIME);
			} catch (InterruptedException e) {
				// Workaround for "Must have at least one statement" warning.
				continue; 
			}
		}
	}
	
	@Override
	public void clearUp(Runner runInstance) {
		super.clearUp(runInstance);
		if (navi != null)
			navi.clearPath();
	}
}
