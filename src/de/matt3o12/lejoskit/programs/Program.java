package de.matt3o12.lejoskit.programs;

import de.matt3o12.lejoskit.Runner;


/**
 * A program controls the behavior of one specific
 * task of the robot.  
 * 
 * For instance, if you want to make the robot walk,
 * you will need to add a new class extending {@link Program}
 * and the code the robot needs to walk in {@link #go(Runner)}.  
 * Finally, you will need to add the subclass you created to
 * a {@link Runner} class that executes your code properly. 
 * 
 * Don't forget that {@link Program} has subclasses that might
 * already cover what you want to do.  
 * For instance, if you simply want to display something, use
 * the {@link DialogProgram} instead.
 */
public abstract class Program {
	
	/**
	 * This method contains the code your robot needs to 
	 * do its thing.
	 * 
	 * @param runInstance The {@link Runner} to use.
	 */
	public abstract void go(Runner runInstance);
	
	/**
	 * Clears the instance up (removes listeners, etc).  
	 * It is called by the {@link Runner} instance when
	 * {@link #go(Runner)} has finished.
	 * 
	 * @param runInstance The {@link Runner} to use.
	 */
	public void clearUp(Runner runInstance) {
		
	}
	
	/**
	 * Called when a program has completed. Does by default nothing.
	 * 
	 * @param event the event to tell the {@link Runner} what to do.
	 */
	public void onCompletion(CompletionEvent event) {
		
	}
}


