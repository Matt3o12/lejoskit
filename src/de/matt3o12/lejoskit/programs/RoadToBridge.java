package de.matt3o12.lejoskit.programs;

import lejos.nxt.LCD;
import lejos.nxt.LightSensor;
import lejos.robotics.navigation.Navigator;
import lejos.robotics.navigation.Waypoint;
import lejos.robotics.pathfinding.Path;
import de.matt3o12.lejoskit.SensorPortName;
import de.matt3o12.lejoskit.SensorUtility;
import de.matt3o12.lejoskit.config.Config;

/**
 * {@link Program} that navigates the robot to the bridge.
 */
public class RoadToBridge extends NaviProgram {
	private Waypoint lastWaypoint;
	private LightSensor leftLightSensor;
	private LightSensor rightLightSensor;
	
	private static final int WAYPOINT1_X = 50;
	private static final int WAYPOINT1_Y = 0;
	
	private static final float WAYPOINT2_X = 50f;
	private static final float WAYPOINT2_Y = Float.POSITIVE_INFINITY;
	
	/**
	 * Creates a new {@link RoadToBridge} program.
	 * 
	 * @param config the {@link Config} to use for loading the ports.
	 * @deprecated use {@link #RoadToBridge()} instead.
	 */
	@Deprecated
	public RoadToBridge(Config config) {
		rightLightSensor = getLightSensorFromConfig(config, "lightSensorRight");
		leftLightSensor = getLightSensorFromConfig(config, "lightSensorLeft");
	}
	
	/**
	 * Creates a new {@link RoadToBridge} program.
	 */
	public RoadToBridge() {
		this(Config.getInstance());
	}
	
	private LightSensor getLightSensorFromConfig(Config config, String property) {
		SensorPortName name = SensorPortName.getPortByName(config.getProperty(property)); 
		return new LightSensor(name.getSensorPort());
	}
	
	@Override
	public Path getPath() {
		Path path = new Path();
		path.add(createWaypoint(WAYPOINT1_X, WAYPOINT1_Y));
		
		lastWaypoint = createWaypoint(WAYPOINT2_X, WAYPOINT2_Y);
		path.add(lastWaypoint);
		
		return path;
	}
	
	/**
	 * Returns if the last way point is reached
	 * (needed by {@link #shouldMotorStop(Navigator)}).
	 * 
	 * @param navi The {@link Navigator} that returns the last waypoint.
	 * @return true if the last way point is reached.
	 */
	private boolean isLastWaypointReached(Navigator navi) {
		return navi.getWaypoint() == lastWaypoint;
	}
	
	@Override
	public boolean shouldStop() {
		Navigator navi = getNavi(); // TODO: remove null.
		
		// TODO: Remove debug info
		LCD.drawString(lastWaypoint.x + " " + lastWaypoint.y, 0, 0);
		LCD.drawString(navi.getWaypoint().x + " " + navi.getWaypoint().y, 0, 1);
		LCD.drawInt((int) System.currentTimeMillis(), 0, 2);
		
		if (!isLastWaypointReached(navi))
			return super.shouldStop();
		
		System.out.println(SensorUtility.isFloorBlack(leftLightSensor));
		if (SensorUtility.isFloorBlack(leftLightSensor) 
				|| SensorUtility.isFloorBlack(rightLightSensor))
			return true;

		return false;
	}
}
