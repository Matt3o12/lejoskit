package de.matt3o12.lejoskit.programs;

import de.matt3o12.lejoskit.MessageTooLongException;
import de.matt3o12.lejoskit.dialog.OkMessageDialog;
import de.matt3o12.lejoskit.dialog.SimpleDialog;

/**
 * Shows the user manual on the display.
 */
public class ShowManual extends SimpleDialogProgram {
	private SimpleDialog simpleDialog;
	
	/**
	 * Creates the {@link ShowManual} {@link Program}.
	 */
	public ShowManual() {
		String s = "Please place the NXT on the right bottom and "
				+ "hit the orange key to start";
		try {
			simpleDialog = new OkMessageDialog(s);
		} catch (MessageTooLongException e) {
			throw new RuntimeException(e);
			// Should never happen because the string does fit on the screen.
			// It was not created dynamically.
		}
	}

	@Override
	public SimpleDialog getSimpleDialog() {
		return simpleDialog;
	}
}
