package de.matt3o12.lejoskit.programs;

import de.matt3o12.lejoskit.dialog.Dialog;
import de.matt3o12.lejoskit.dialog.DialogListener;
import de.matt3o12.lejoskit.dialog.SimpleDialog;

/**
 * Program to display a simple dialog. For more information how 
 * it is display, see: {@link DialogProgram}. 
 */
public abstract class SimpleDialogProgram extends DialogProgram {
	/**
	 * Returns the {@link SimpleDialog} to display. Make sure this method
	 * always returns the same dialog. Cache the dialog somewhere.
	 * 
	 * @return the dialog to return.
	 */
	public abstract SimpleDialog getSimpleDialog();
	
	@Override
	public Dialog getDialog() {
		return getSimpleDialog();
	}

	@Override
	public DialogListener getDialogListener() {
		return getSimpleDialog();
	}

}
