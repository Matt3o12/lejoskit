package de.matt3o12.lejoskit;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;

import java.io.File;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Spy;

import de.matt3o12.lejoskit.CrashReporter;

public class CrashReporterTest {
	@Spy private CrashReporter reporter;
	
	@Before
	public void setUp() throws Exception {
		reporter = spy(new CrashReporter(null));
	}

	@Test
	public void test_getCrashFileName1() {
		stub_getFiles(new File[0]);
		
		assertEquals("crash_report1.txt", reporter.getCrashFileName());
	}

	@Test
	public void test_getCrashFileName2() {
		stub_getFiles("crash_report1.txt");
		
		assertEquals("crash_report2.txt", reporter.getCrashFileName());
	}

	@Test
	public void test_getCrashFileName3() {
		stub_getFiles("crash_report1.txt", "crash_report2.txt");
		
		assertEquals("crash_report3.txt", reporter.getCrashFileName());
	}

	@Test
	public void test_getCrashFileName4() {
		stub_getFiles("crash_report1.txt", "crash_report2.txt",
				      "crash_report3.txt", "crash_report4.txt",
				      "crash_report5.txt", "crash_report6.txt",
				      "crash_report7.txt", "crash_report8.txt");
		
		assertEquals("crash_report5.txt", reporter.getCrashFileName());
	}
	
	@Test
	public void test_getCrashFileName5() {
		stub_getFiles("crash_report1.txt", "crash_report3.txt");
		
		assertEquals("crash_report4.txt", reporter.getCrashFileName());
	}

	@Test
	public void test_getCrashFileName6() {
		stub_getFiles("blah.txt", "crash_report2.txt");
		
		assertEquals("crash_report3.txt", reporter.getCrashFileName());
	}
	
	@Test
	public void test_getCrashFileName7() {
		stub_getFiles("blah.txt", "crash_report1.txt", "run.nxj");
		
		assertEquals("crash_report2.txt", reporter.getCrashFileName());
	}
	
	@Test
	public void test_getCrashFileName8() {
		stub_getFiles("adfg.txt", "run.sdfg");
		
		assertEquals("crash_report1.txt", reporter.getCrashFileName());
	}
	
	public void stub_getFiles(File... files) {
		doReturn(files).when(reporter).getFiles();
	}
	
	public void stub_getFiles(String... files) {
		File[] result = new File[files.length];
		for (int i = 0; i < files.length; i++) {
			String fileName = files[i];
			File file = mock(File.class);
			doReturn(fileName).when(file).getName();
			result[i] = file;
		}
		
		stub_getFiles(result);
	}
}
