package de.matt3o12.lejoskit;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import de.matt3o12.lejoskit.DisplayUnit;
import de.matt3o12.lejoskit.MessageTooLongException;



public class DisplayUtilTest {
	private DisplayUnit display;
	
	@Before
	public void setUp() {
		display = new DisplayUnit();
	}

	@Test
	public void test_splitString_trimming() {
		String[] result = display.splitString("te st");
		assertEquals("te st", result[0]);

		result = display.splitString("te   st");
		assertEquals("te st", result[0]);

		result = display.splitString("te t   st  st");
		assertEquals("te t st st", result[0]);
		
		result = display.splitString("  test");
		assertEquals("test", result[0]);

		result = display.splitString("test   ");
		assertEquals("test", result[0]);
		
		result = display.splitString("  te s  t            t");
		assertEquals("te s t t", result[0]);
		assertEquals(null, result[1]);
	}
	
	@Test
	public void test_splitString_emptyString(){
		String[] result = display.splitString("");
		
		assertEquals(null, result[0]);
	}
	
	@Test
	public void test_splitString1() {
		String s = "Hello word I'm a long text and I'm not going to end. I hope this will work properly";
		String[] result = display.splitString(s);
		
		String[] expectedResult = new String[8];
		expectedResult[0] = "Hello word I'm a";
		expectedResult[1] = "long text and";
		expectedResult[2] = "I'm not going to";
		expectedResult[3] = "end. I hope this";
		expectedResult[4] = "will work";
		expectedResult[5] = "properly";

		assertArrayEquals(expectedResult, result);
	}
	
	@Test
	public void test_splitString2() {
		String s = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras viverra tortor non risus scelerisque condimentum.";
		String[] result = display.splitString(s);
		
		String[] expectedResult = new String[8];
		expectedResult[0] = "Lorem ipsum";
		expectedResult[1] = "dolor sit amet,";
		expectedResult[2] = "consectetur";
		expectedResult[3] = "adipiscing elit.";
		expectedResult[4] = "Cras viverra";
		expectedResult[5] = "tortor non risus";
		expectedResult[6] = "scelerisque con-";
		expectedResult[7] = "dimentum.";

		assertArrayEquals(expectedResult, result);
	}
	
	@Test(expected = ArrayIndexOutOfBoundsException.class)
	public void test_splitString_exception() {
		String s = "Hello Word I'm a long text and I'm not going to end. I hope this will not work because this text will most likely exceed the screen size";
		display.splitString(s);
	}
	
	@Test
	public void test_splitString_tooLong1(){
		String s = "tea abbreviation";
		String[] result = display.splitString(s);
		
		String[] expectedResult = new String[8];
		expectedResult[0] = "tea abbreviation";

		assertArrayEquals(expectedResult, result);
	}	
	
	@Test
	public void test_splitString_tooLong2(){
		String s = "test abbreviation";
		String[] result = display.splitString(s);
		
		String[] expectedResult = new String[8];
		expectedResult[0] = "test abbreviati-";
		expectedResult[1] = "on";

		assertArrayEquals(expectedResult, result);
	}	
	
	@Test
	public void test_splitString_tooLong3(){
		String s = "ahead abbreviation";
		String[] result = display.splitString(s);
		
		String[] expectedResult = new String[8];
		expectedResult[0] = "ahead abbreviat-";
		expectedResult[1] = "ion";

		assertArrayEquals(expectedResult, result);
	}	
	
	@Test
	public void test_splitString_tooLong4(){
		String s = "te abbreviations";
		String[] result = display.splitString(s);
		
		String[] expectedResult = new String[8];
		expectedResult[0] = "te abbreviations";

		assertArrayEquals(expectedResult, result);
	}	
	
	@Test
	public void test_splitString_tooLong5(){
		String s = "abbreviation te";
		String[] result = display.splitString(s);
		
		String[] expectedResult = new String[8];
		expectedResult[0] = "abbreviation te";

		assertArrayEquals(expectedResult, result);
	}	
	
	@Test
	public void test_splitString_tooLong6(){
		String s = "character abbreviation";
		String[] result = display.splitString(s);
		
		String[] expectedResult = new String[8];
		expectedResult[0] = "character abbre-";
		expectedResult[1] = "viation";

		assertArrayEquals(expectedResult, result);
	}	
	
	@Test
	public void test_splitString_superlong1() {
		String s = "iamasuperlongwordthatdoesntappeartoendbecauseitissuperlonganddoesnthavespaces";
		String[] result = display.splitString(s);
		
		String[] expectedResult = new String[8];
		expectedResult[0] = "iamasuperlongwo-";
		expectedResult[1] = "rdthatdoesntapp-";
		expectedResult[2] = "eartoendbecause-";
		expectedResult[3] = "itissuperlongan-";
		expectedResult[4] = "ddoesnthavespac-";
		expectedResult[5] = "es";

		assertArrayEquals(expectedResult, result);
	}	
	
	@Test
	public void test_splitString_superlong2() {
		String s = "test iamasuperlongwordthatdoesntappeartoendbecauseitissuperlonganddoesnthavespaces bl blahhhh";
		String[] result = display.splitString(s);
		
		String[] expectedResult = new String[8];
		expectedResult[0] = "test iamasuperl-";
		expectedResult[1] = "ongwordthatdoes-";
		expectedResult[2] = "ntappeartoendbe-";
		expectedResult[3] = "causeitissuperl-";
		expectedResult[4] = "onganddoesnthav-";
		expectedResult[5] = "espaces bl";
		expectedResult[6] = "blahhhh";

		assertArrayEquals(expectedResult, result);
	}		
	@Test
	public void test_splitString_superlong3() {
		String s = "dxqjhhrvoxihrnwyfforqvjpxwirbvbjwkhkqxhapeagogdrbaqdnhfzvckbyshflxrnrashowqlleqxgwjkuunshwkdoeqjtjfnvbnyjanljrboggwscubfm";
		String[] result = display.splitString(s);
		
		String[] expectedResult = new String[8];
		expectedResult[0] = "dxqjhhrvoxihrnw-";
		expectedResult[1] = "yfforqvjpxwirbv-";
		expectedResult[2] = "bjwkhkqxhapeago-";
		expectedResult[3] = "gdrbaqdnhfzvckb-";
		expectedResult[4] = "yshflxrnrashowq-";
		expectedResult[5] = "lleqxgwjkuunshw-";
		expectedResult[6] = "kdoeqjtjfnvbnyj-";
		expectedResult[7] = "anljrboggwscubfm";

		assertArrayEquals(expectedResult, result);
	}
	
	@Test
	public void test_splitString_superlongWithMaxLength() {
		String s = "dxqjhhrvoxihrnwyfforqvjpxwirbvbjwkhkqxhapeagogdrbaqdnhfzvckbyshflxrnrashowql";
		String[] result = display.splitString(s, 5);
		
		String[] expectedResult = new String[5];
		expectedResult[0] = "dxqjhhrvoxihrnw-";
		expectedResult[1] = "yfforqvjpxwirbv-";
		expectedResult[2] = "bjwkhkqxhapeago-";
		expectedResult[3] = "gdrbaqdnhfzvckb-";
		expectedResult[4] = "yshflxrnrashowql";

		assertArrayEquals(expectedResult, result);
	}
	
	@Test(expected=ArrayIndexOutOfBoundsException.class)
	public void test_splitString_length_tooLong1() {
		display.splitString("dxqjhhrvoxihrnwyfforqvjpxwirbvbjwkhkqxhapeago", 1);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void test_splitString_length_invalid1() {
		display.splitString("", 0);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void test_splitString_length_invalid2() {
		display.splitString("", -1);
	}
	
	@Test
	public void test_splitString_length_valid() {
		display.splitString("", 1);
	}
	
	@Test
	public void test_splitString_length1() {
		String[] result = display.splitString("hello world", 1);
				
		assertEquals(1, result.length);
		assertEquals("hello world", result[0]);
	}
	
	@Test
	public void test_splitString_length2() {
		String[] result = display.splitString("hello world", 2);
				
		assertEquals(2, result.length);
		assertEquals("hello world", result[0]);
		assertEquals(null, result[1]);
	}
	
	
	@Test
	public void test_splitString_length3() {
		String[] result = display.splitString("hello world. Does it work?", 2);
				
		assertEquals(2, result.length);
		assertEquals("hello world.", result[0]);
		assertEquals("Does it work?", result[1]);
	}
	
	@Test
	public void test_splitString_noLength() {
		String[] result = display.splitString("");
		
		assertEquals(8, result.length);
		assertEquals(null, result[0]);
		assertEquals(null, result[1]);
		assertEquals(null, result[2]);
		assertEquals(null, result[3]);
		assertEquals(null, result[4]);
		assertEquals(null, result[5]);
		assertEquals(null, result[6]);
		assertEquals(null, result[7]);
	}
	
	@Test
	@SuppressWarnings("deprecation")
	public void test_makeDialog1() throws Exception {
		String[] results = display.makeDialog("Hello World", new String[] {"Ok", "Cancel"});
		
		String[] expectedResult = new String[8];
		expectedResult[0] = "Hello World";
		expectedResult[7] = "Ok        Cancel";
		assertArrayEquals(expectedResult, results);
	}
	
	@Test
	@SuppressWarnings("deprecation")
	public void test_makeDialog2() throws Exception {
		String s = "I'm a string over mutiple lines";
		String[] results = display.makeDialog(s, new String[] {"abcdefghijklmn", "o"});
		
		String[] expectedResult = new String[8];
		expectedResult[0] = "I'm a string";
		expectedResult[1] = "over mutiple";
		expectedResult[2] = "lines";
		expectedResult[7] = "abcdefghijklmn o";
		assertArrayEquals(expectedResult, results);
	}
	
	@Test
	@SuppressWarnings("deprecation")
	public void test_makeDialog3() throws Exception {
		String s = "VD5VnxHbQy8Dw1mMCFCdxUaSEWF71h4XjnyJaZXEstmLzuDP47M3ThGMoaQjMCFCdxUaSEWF71hcodnO5Z7MDrroLqm";
		String[] results = display.makeDialog(s, new String[] {"o", "abcdefghijklmn"});
		
		String[] expectedResult = new String[8];
		expectedResult[0] = "VD5VnxHbQy8Dw1m-";
		expectedResult[1] = "MCFCdxUaSEWF71h-";
		expectedResult[2] = "4XjnyJaZXEstmLz-";
		expectedResult[3] = "uDP47M3ThGMoaQj-";
		expectedResult[4] = "MCFCdxUaSEWF71h-";
		expectedResult[5] = "codnO5Z7MDrroLqm";
		expectedResult[7] = "o abcdefghijklmn";
		assertArrayEquals(expectedResult, results);
	}
	
	@SuppressWarnings("deprecation")
	@Test
	public void test_makeDialog_validInput() throws Exception {
		display.makeDialog("", new String[] {"Ok", "Cancel"});
		display.makeDialog("", new String[] {"abcdefghijklmn", "o"});
		display.makeDialog("", new String[] {"Ok", "Cancel"});
		display.makeDialog("", new String[] {"Ok", "Cancel"});
	}
	
	@Test(expected=IllegalArgumentException.class)
	@SuppressWarnings("deprecation")
	public void test_makeDialog_invalidInput1() throws Exception {
		display.makeDialog("", new String[] {"Ok"});
	}
	
	@Test(expected=IllegalArgumentException.class)
	@SuppressWarnings("deprecation")
	public void test_makeDialog_invalidInput2() throws Exception {
		display.makeDialog("", new String[] {"A", "B", "C"});
	}
	
	@Test(expected=MessageTooLongException.class)
	@SuppressWarnings("deprecation")
	public void test_makeDialog_inputTooLong1() throws Exception {
		display.makeDialog("", new String[] {"abcdefghijklmn", "of"});
	}
	
	@Test(expected=MessageTooLongException.class)
	@SuppressWarnings("deprecation")
	public void test_makeDialog_inputTooLong2() throws Exception {
		display.makeDialog("", new String[] {"abcdefghijklmn", "abcdefghijklmn"});
	}
	
	@Test
	public void test_centerString_empty() {
		assertEquals("        ", display.centerString(""));
	}
	
	
	@Test
	public void test_centerString1() {
		assertEquals("       aa", display.centerString("aa"));
	}
	
	
	@Test
	public void test_centerString2() {
		assertEquals("       a", display.centerString("a"));
	}
	
	
	@Test
	public void test_centerString3() {
		assertEquals("     abcdef", display.centerString("abcdef"));
	}
	
	@Test
	public void test_centerString4() {
		assertEquals("    abcdefg", display.centerString("abcdefg"));
	}
	
	@Test
	public void test_centerString_tooLong() {
		assertEquals("1234567890123456789", display.centerString("1234567890123456789"));
	}
	
	public void test_centerString1_inverted() {
		assertEquals("       \\iaa", display.centerString("\\iaa"));
	}
	
	
	@Test
	public void test_centerString2_inverted() {
		assertEquals("       a\\i", display.centerString("a\\i"));
	}
	
	
	@Test
	public void test_centerString3_inverted() {
		assertEquals("     ab\\icd\\ief", display.centerString("ab\\icd\\ief"));
	}
	
	@Test
	public void test_centerString4_inverted() {
		assertEquals(" 12\\i3456\\i78901234", display.centerString("12\\i3456\\i78901234"));
	}
	
	@Test
	public void test_clearRow() throws Exception {
		String[][] rows = new String[][] {
			new String[] {
				"Hello World",
				"Hello World"
			},
			
			new String[] {
					"",
					""
			},
			
			new String[] {
					"\\i",
					""
			},
			
			new String[] {
					"\\i\\i",
					""
			}
		};
		
		for (String[] row : rows) {
			assertEquals(row[1], DisplayUnit.clearRow(row[0]));
		}
	}
}
