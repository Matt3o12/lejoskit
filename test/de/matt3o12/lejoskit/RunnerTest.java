package de.matt3o12.lejoskit;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import org.junit.Test;

import de.matt3o12.lejoskit.Runner;
import de.matt3o12.lejoskit.programs.Program;

public class RunnerTest {
	
	@Test
	public void test_init() {
		@SuppressWarnings("deprecation")
		Runner run = new Runner(false, null);
		assertTrue(run.getPrograms().isEmpty());
	}
	
	@Test
	public void test_addProgram() {
		@SuppressWarnings("deprecation")
		Runner run = new Runner(false, null);
		Program mockedProgram1 = mock(Program.class);
		Program mockedProgram2 = mock(Program.class);
		Program mockedProgram3 = mock(Program.class);
		
		run.addProgram(mockedProgram1);
		assertEquals(1, run.getPrograms().size());
		assertEquals(mockedProgram1, run.getPrograms().get(0));

		run.addProgram(mockedProgram2);
		run.addProgram(mockedProgram3);
		assertEquals(3, run.getPrograms().size());
		assertEquals(mockedProgram1, run.getPrograms().get(0));
		assertEquals(mockedProgram2, run.getPrograms().get(1));
		assertEquals(mockedProgram3, run.getPrograms().get(2));
	}
	
	@Test
	public void test_start() throws Exception {
		@SuppressWarnings("deprecation")
		Runner run = new Runner(false, null);
		Program mockedProgram1 = mock(Program.class);
		Program mockedProgram2 = mock(Program.class);
		Program mockedProgram3 = mock(Program.class);
		Program mockedProgram4 = mock(Program.class);
		run.addProgram(mockedProgram1);
		run.addProgram(mockedProgram2);
		run.addProgram(mockedProgram4);
		run.start();
		run.addProgram(mockedProgram3);
		
		verify(mockedProgram1).go(run);
		verify(mockedProgram1).clearUp(run);
		verify(mockedProgram2).go(run);
		verify(mockedProgram2).clearUp(run);
		verify(mockedProgram4).go(run);
		verify(mockedProgram4).clearUp(run);
		verify(mockedProgram3, never()).go(any(Runner.class));
		verify(mockedProgram3, never()).clearUp(any(Runner.class));
	}
}
