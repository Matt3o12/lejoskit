package de.matt3o12.lejoskit;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import lejos.nxt.LightSensor;

import org.junit.Before;
import org.junit.Test;

import de.matt3o12.lejoskit.SensorUtility;

public class SensorUtilityTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void test_isFloorBlack_lightOn() {
		LightSensor sensor = mock(LightSensor.class);
		doReturn(true).when(sensor).isFloodlightOn();
		
		doReturn(0).when(sensor).getNormalizedLightValue();
		assertEquals(true, SensorUtility.isFloorBlack(sensor));

		doReturn(300).when(sensor).getNormalizedLightValue();
		assertEquals(true, SensorUtility.isFloorBlack(sensor));
		
		doReturn(700).when(sensor).getNormalizedLightValue();
		assertEquals(false, SensorUtility.isFloorBlack(sensor));
		
		doReturn(1200).when(sensor).getNormalizedLightValue();
		assertEquals(false, SensorUtility.isFloorBlack(sensor));

		
		doReturn(451).when(sensor).getNormalizedLightValue();
		assertEquals(false, SensorUtility.isFloorBlack(sensor));

		doReturn(450).when(sensor).getNormalizedLightValue();
		assertEquals(true, SensorUtility.isFloorBlack(sensor));
		
		doReturn(449).when(sensor).getNormalizedLightValue();
		assertEquals(true, SensorUtility.isFloorBlack(sensor));
	}

	@Test
	public void test_isFloorBlack_lightOff() {
		LightSensor sensor = mock(LightSensor.class);
		doReturn(false).when(sensor).isFloodlightOn();
		
		doReturn(0).when(sensor).getNormalizedLightValue();
		assertEquals(true, SensorUtility.isFloorBlack(sensor));

		doReturn(100).when(sensor).getNormalizedLightValue();
		assertEquals(true, SensorUtility.isFloorBlack(sensor));
		
		doReturn(400).when(sensor).getNormalizedLightValue();
		assertEquals(false, SensorUtility.isFloorBlack(sensor));
		
		doReturn(1200).when(sensor).getNormalizedLightValue();
		assertEquals(false, SensorUtility.isFloorBlack(sensor));

		
		doReturn(201).when(sensor).getNormalizedLightValue();
		assertEquals(false, SensorUtility.isFloorBlack(sensor));

		doReturn(200).when(sensor).getNormalizedLightValue();
		assertEquals(true, SensorUtility.isFloorBlack(sensor));
		
		doReturn(199).when(sensor).getNormalizedLightValue();
		assertEquals(true, SensorUtility.isFloorBlack(sensor));
	}

}
