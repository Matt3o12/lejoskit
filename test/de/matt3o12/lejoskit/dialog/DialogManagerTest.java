package de.matt3o12.lejoskit.dialog;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import de.matt3o12.lejoskit.dialog.DialogListener;
import de.matt3o12.lejoskit.dialog.DialogManager;

public class DialogManagerTest {
	private class MockedDilogManager extends DialogManager {
		@Override
		protected void addButtonListeners() {
			
		}
	}
	
	private DialogManager dialogManager;
	
	@Before
	public void setUp() throws Exception {
		dialogManager = new MockedDilogManager();
	}

	@Test
	public void test_addDialogListener() {
		assertTrue(dialogManager.getListeners().isEmpty());
		
		DialogListener listener1 = mock(DialogListener.class);
		DialogListener listener2 = mock(DialogListener.class);
		DialogListener listener3 = mock(DialogListener.class);
		
		dialogManager.addListener(listener1);
		assertEquals(1, dialogManager.getListeners().size());
		assertEquals(listener1, dialogManager.getListeners().get(0));
		
		ArrayList<DialogListener> expectedListeners = new ArrayList<DialogListener>();
		expectedListeners.add(listener1);
		expectedListeners.add(listener2);
		expectedListeners.add(listener3);
		
		dialogManager.addListener(listener2);
		dialogManager.addListener(listener3);
		
		assertEquals(expectedListeners, dialogManager.getListeners());
	}
	
	@Test
	public void test_removeDialogListener() throws Exception {
		// Make sure test_addDialogListener works.
		DialogListener listener1 = mock(DialogListener.class);
		DialogListener listener2 = mock(DialogListener.class);
		DialogListener listener3 = mock(DialogListener.class);

		dialogManager.addListener(listener1);
		dialogManager.addListener(listener2);
		dialogManager.addListener(listener3);
		
		assertEquals(3, dialogManager.getListeners().size());
		
		assertTrue(dialogManager.removeListener(listener2));
		
		ArrayList<DialogListener> expectedListeners = new ArrayList<DialogListener>();
		expectedListeners.add(listener1);
		expectedListeners.add(listener3);
		
		assertEquals(expectedListeners, dialogManager.getListeners());
		
		assertTrue(dialogManager.removeListener(listener1));
		assertTrue(dialogManager.removeListener(listener3));
		assertTrue(dialogManager.getListeners().isEmpty());
	}
	
	@Test
	public void test_removeDialogListener1() throws Exception {
		DialogListener listener = mock(DialogListener.class);
		assertFalse(dialogManager.removeListener(listener));
	}
	
//	@Test
//	public void test_buttonEvent_left() {
//		DialogListener listener = mock(DialogListener.class);
//		Button button = mock(Button.class); // Cannot mock button.
//		
//		when(button.getId()).thenReturn(Button.ID_LEFT);
//		
//		dialogManager.addListener(listener);
//		dialogManager.buttonEvent(button);
//		
//		verify(listener).onLeftPress();
//		verify(listener).onRightPress();
//		verify(listener).onOkPress();
//		verify(listener).onCancelPress();
//	}
}
