package de.matt3o12.lejoskit.dialog;

import static org.junit.Assert.*;

import org.junit.Test;

import de.matt3o12.lejoskit.MessageTooLongException;
import de.matt3o12.lejoskit.dialog.OkMessageDialog;

public class OkMessageDialogTest {
	@Test
	public void test_getRows() throws MessageTooLongException {
		OkMessageDialog dialog = new OkMessageDialog("Hello world");
		String[] result = dialog.getRows();
		
		String[] expectedResult = new String[8];
		expectedResult[0] = "Hello world";
		expectedResult[7] = "       Ok";
		
		assertArrayEquals(expectedResult, result);
	}

}
