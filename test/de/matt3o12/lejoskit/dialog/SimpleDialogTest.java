package de.matt3o12.lejoskit.dialog;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doCallRealMethod;
import static org.mockito.Mockito.mock;

import org.junit.Before;
import org.junit.Test;

import de.matt3o12.lejoskit.dialog.SimpleDialog;
import de.matt3o12.lejoskit.dialog.SimpleDialogResult;

public class SimpleDialogTest {
	private SimpleDialog simpleDialog;
	
	@Before
	public void setUp() throws Exception {
		simpleDialog = mock(SimpleDialog.class);
	}

	@Test
	public void test_getResult_cancel() {
		doCallRealMethod().when(simpleDialog).getResult();
		doCallRealMethod().when(simpleDialog).setResult(SimpleDialogResult.CANCEL);
		doCallRealMethod().when(simpleDialog).onCancelPress();
		
		simpleDialog.onCancelPress();
		assertEquals(SimpleDialogResult.CANCEL, simpleDialog.getResult());
	}

	@Test
	public void test_getResult_ok() {
		doCallRealMethod().when(simpleDialog).getResult();
		doCallRealMethod().when(simpleDialog).setResult(SimpleDialogResult.OK);
		doCallRealMethod().when(simpleDialog).onOkPress();
		
		simpleDialog.onOkPress();
		assertEquals(SimpleDialogResult.OK, simpleDialog.getResult());
	}

	@Test
	public void test_getResult_left() {
		doCallRealMethod().when(simpleDialog).getResult();
		doCallRealMethod().when(simpleDialog).setResult(SimpleDialogResult.LEFT_INPUT);
		doCallRealMethod().when(simpleDialog).onLeftPress();
		
		simpleDialog.onLeftPress();
		assertEquals(SimpleDialogResult.LEFT_INPUT, simpleDialog.getResult());
	}

	@Test
	public void test_getResult_right() {
		doCallRealMethod().when(simpleDialog).getResult();
		doCallRealMethod().when(simpleDialog).setResult(SimpleDialogResult.RIGHT_INPUT);
		doCallRealMethod().when(simpleDialog).onRightPress();
		
		simpleDialog.onRightPress();
		assertEquals(SimpleDialogResult.RIGHT_INPUT, simpleDialog.getResult());
	}
}
