package de.matt3o12.lejoskit.programs;

import static org.mockito.Mockito.*;


import org.junit.Before;
import org.junit.Test;

import de.matt3o12.lejoskit.Runner;
import de.matt3o12.lejoskit.dialog.Dialog;
import de.matt3o12.lejoskit.dialog.DialogListener;
import de.matt3o12.lejoskit.dialog.DialogManager;
import de.matt3o12.lejoskit.programs.DialogProgram;

public class DialogProgramTest {
	private class DeprecatedDialogProgram extends DialogProgram {
		private Dialog dialog;
		private DialogListener listener;
		
		public DeprecatedDialogProgram(Dialog dialog, DialogListener listener) {
			super();
			
			this.dialog = dialog;
			this.listener = listener;
		}

		@Override
		public Dialog getDialog() {
			return dialog;
		}

		@Override
		public DialogListener getDialogListener() {
			return listener;
		}
	}
	
	private Runner runInstance;
	private DialogManager dialogManager;
	private Dialog dialog;
	private DialogListener dialogListener;
	private DialogProgram dialogProgram;
	
	@Before
	public void setUp() throws Exception {
		runInstance = mock(Runner.class);
		dialogManager = mock(DialogManager.class);
		dialog = mock(Dialog.class);
		dialogListener = mock(DialogListener.class);
		
		dialogProgram = spy(new DeprecatedDialogProgram(dialog, dialogListener));
	}

	@Test(timeout=900)
	public void test_waitForUser() {
		doReturn(false).doReturn(true).when(dialog).isDone();		
		dialogProgram.waitForUser();
		
		verify(dialog, times(2)).isDone();
	}
	
	@Test
	public void test_go() throws Exception {
		doNothing().when(dialogProgram).waitForUser();
		doReturn(dialogManager).when(runInstance).getDialogManager();
		dialogProgram.go(runInstance);
		
		verify(dialogManager).addListener(dialogListener);
		verify(dialog).showDialog();
	}
	
//	The clearUp test is no longer valid since dialogProgram.clearUp uses a native NXT
//	method which will break the test.
//	@Test
//	public void test_clearUp() {
//		doReturn(dialogManager).when(runInstance).getDialogManager();
//		doReturn(true).when(dialogManager).removeListener(any(DialogListener.class));
//		
//		dialogProgram.clearUp(runInstance);
//		verify(dialogManager).removeListener(dialogListener);
//	}
}
